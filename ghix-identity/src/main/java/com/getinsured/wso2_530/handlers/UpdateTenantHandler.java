package com.getinsured.wso2_530.handlers;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.im.wso2.exceptions.ValidationException;
import com.getinsured.wso2_530.wrapper.UpdateTenantWrapper;

public class UpdateTenantHandler extends AbstractServiceHandler {
	private boolean handleResponseCalled = false;
	@Override
	public Object getRequestpayLoad() throws HubServiceException, HubMappingException {
		JSONParser parser = new JSONParser();
		try {
			JSONObject addTenantRequest = (JSONObject) parser.parse(this.getJsonInput());
			String userName = (String) addTenantRequest.get("adminName");
			String adminEmail = (String) addTenantRequest.get("adminEmail");
			String password = (String) addTenantRequest.get("adminPassword");
			String tenantDomain = (String) addTenantRequest.get("tenantDomain");
			String tenantIdStr = (String) addTenantRequest.get("tenantId");
			String adminFirstName = (String) addTenantRequest.get("firstName");
			String lastName = (String) addTenantRequest.get("lastName");
			UpdateTenantWrapper wrapper = new UpdateTenantWrapper();
			wrapper.setFirstName(adminFirstName);
			wrapper.setLastName(lastName);
			wrapper.setAdminName(userName);
			wrapper.setActive(true);
			wrapper.setAdminPassword(password);
			wrapper.setAdminEmail(adminEmail);
			wrapper.setTenantDomain(tenantDomain);
			if(tenantIdStr != null){
				try{
					int tenantId = Integer.parseInt(tenantIdStr);
					wrapper.setTenantId(tenantId);
				}catch(NumberFormatException ne){
					// Ignoring, we'll be assigned an auto generated tenant Id
				}
			}
			return wrapper.getSystemRepresentation();
		} catch (ParseException | ValidationException e) {
			throw new HubServiceException("Failed to construct the payload from request input "+e.getMessage());
		}
	}

	@Override
	public String handleResponse(Object responseObject) throws HubServiceException {
		
		handleResponseCalled  = true;
		return "Success";
	}

	@Override
	public RequestStatus getRequestStatus() {
		if(this.handleResponseCalled){
			return RequestStatus.SUCCESS;
		}
		return RequestStatus.FAILED;
	}

	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Marshaller getServiceMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

}
