package com.getinsured.wso2_530.handlers;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.wso2_530.ClaimValueNotSupported;
import com.getinsured.wso2_530.wrapper.SetUserClaimValueWrapper;
public class SetUserClaimHandler extends AbstractServiceHandler {

	private boolean handleResponseCalled = false;

	@Override
	public Object getRequestpayLoad() throws HubServiceException,
			HubMappingException {
		JSONParser parser = new JSONParser();
		try {
			JSONObject updateUserJsonrequest = (JSONObject) parser.parse(this.getJsonInput());
			String userName = (String) updateUserJsonrequest.get("userName");
			String claimUri = (String) updateUserJsonrequest.get("claimURI");
			String claimValue = (String) updateUserJsonrequest.get("claimValue");
			SetUserClaimValueWrapper wrapper = new SetUserClaimValueWrapper();
			wrapper.setTargetUser(userName, "default");
			wrapper.setUserClaim(claimUri, claimValue);
			return wrapper.getSystemRepresentation();
		} catch (ParseException | ClaimValueNotSupported e) {
			throw new HubServiceException("Failed to construct the payload from request input",e);
		}
	}

	@Override
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		// TODO Auto-generated method stub
		handleResponseCalled  = true;
		return "Success";
	}

	@Override
	public RequestStatus getRequestStatus() {
		if(this.handleResponseCalled){
			return RequestStatus.SUCCESS;
		}
		return RequestStatus.FAILED;
	}

	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Marshaller getServiceMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

}
