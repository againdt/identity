package com.getinsured.wso2_530.handlers;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import com.getinsured.identity.um.ws.service.AuthenticateResponse;
import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.wso2_530.wrapper.AuthenticateWrapper;
import com.getinsured.wso2_530.wrapper.AuthenticationResponseWrapper;

public class AuthenticateServiceHandler extends AbstractServiceHandler {

	private AuthenticationResponseWrapper responseWrapper;

	@Override
	public Object getRequestpayLoad() throws HubServiceException,
			HubMappingException {
		JSONParser parser = new JSONParser();
		try {
			JSONObject authJsonrequest = (JSONObject) parser.parse(this.getJsonInput());
			String userName = (String) authJsonrequest.get("userName");
			String password = (String) authJsonrequest.get("password");
			AuthenticateWrapper authRequest = new AuthenticateWrapper();
			authRequest.setCredential(password);
			authRequest.setUserName(userName);
			return authRequest.getSystemRepresentation();
		} catch (ParseException e) {
			throw new HubServiceException("Failed to construct the payload from request input");
		}
	}

	@Override
	public String handleResponse(Object response) throws HubServiceException {
		if(!(response instanceof AuthenticateResponse)){
			throw new HubServiceException("Invalid response received expected "+AuthenticateResponse.class.getName()+" Received:"+response.getClass().getName());
		}
		responseWrapper= new AuthenticationResponseWrapper((AuthenticateResponse) response);
		boolean success = responseWrapper.isAuthenticated();
		return Boolean.toString(success);
	}

	@Override
	public RequestStatus getRequestStatus() {
		if(this.responseWrapper == null){
			return RequestStatus.FAILED;
		}
		return RequestStatus.SUCCESS;
	}

	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Marshaller getServiceMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

	

}
