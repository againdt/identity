package com.getinsured.wso2_530;

import javax.xml.bind.JAXBElement;

import com.getinsured.gitenant.stratos.common.beans.xsd.ObjectFactory;
import com.getinsured.gitenant.stratos.common.beans.xsd.TenantInfoBean;

public class TenantInfoBeanAttributesFactory {
	
	
	private static ObjectFactory factory = new ObjectFactory();
	
	public static JAXBElement<String> getFirstNameElement(String value){
		return factory.createTenantInfoBeanFirstname(value);
	}
	
	public static JAXBElement<String> getLastNameElement(String value){
		return factory.createTenantInfoBeanLastname(value);
	}
	
	public static JAXBElement<String> getEmailElement(String value){
		return factory.createTenantInfoBeanEmail(value);
	}
	
	public static JAXBElement<String> getAdminElement(String value){
		return factory.createTenantInfoBeanAdmin(value);
	}
	
	public static JAXBElement<String> getAdminPasswordElament(String value){
		return factory.createTenantInfoBeanAdminPassword(value);
	}
	
	public static JAXBElement<String> getTenantDomainElament(String value){
		return factory.createTenantInfoBeanTenantDomain(value);
	}
	
	public static TenantInfoBean getTenantInfoBean(){
		return factory.createTenantInfoBean();
	}
	
	

}
