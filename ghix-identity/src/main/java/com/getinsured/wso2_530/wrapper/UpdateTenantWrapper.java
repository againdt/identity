package com.getinsured.wso2_530.wrapper;

import javax.xml.bind.JAXBElement;

import com.getinsured.gitenant.stratos.common.beans.xsd.TenantInfoBean;
import com.getinsured.gitenant.tenant.mgt.services.ObjectFactory;
import com.getinsured.gitenant.tenant.mgt.services.UpdateTenant;
import com.getinsured.iex.im.wso2.exceptions.ValidationException;
import com.getinsured.wso2_530.TenantInfoBeanAttributesFactory;


public class UpdateTenantWrapper {
	
	private static ObjectFactory factory = new ObjectFactory();
	private String active = null;
	private String tenantDomain = null;
	private String adminName = null;
	private String adminPassword = null;
	private String firstName = null;
	private String lastName = null;
	private String adminEmail = null;
	private int tenantId;

	
	
	public UpdateTenantWrapper(){
		
	}


	public String getTenantDomain() {
		return tenantDomain;
	}


	public void setTenantDomain(String tenantDomain) {
		this.tenantDomain = tenantDomain;
	}


	public String getAdminName() {
		return adminName;
	}


	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}


	public String getAdminPassword() {
		return adminPassword;
	}


	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getAdminEmail() {
		return adminEmail;
	}


	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}
	
	
	public boolean isActive() {
		return this.active.equalsIgnoreCase("true");
	}


	public void setActive(boolean active) {
		this.active = ((active)?"true":"false");
	}


	public UpdateTenant getUpdateTenantRequest() throws ValidationException{
		UpdateTenant tenantPayLoad = factory.createUpdateTenant();
		TenantInfoBean tenantBean = TenantInfoBeanAttributesFactory.getTenantInfoBean();
		if(active != null){
			tenantBean.setActive(this.isActive());
		}
		if(this.adminName != null){
			tenantBean.setAdmin(TenantInfoBeanAttributesFactory.getAdminElement(this.adminName));
		}
		if(this.adminEmail != null){
			tenantBean.setEmail(TenantInfoBeanAttributesFactory.getEmailElement(this.adminEmail));
		}
		if(this.adminPassword != null){
			tenantBean.setAdminPassword(TenantInfoBeanAttributesFactory.getAdminPasswordElament(this.adminPassword));
		}
		if(this.firstName != null){
			tenantBean.setFirstname(TenantInfoBeanAttributesFactory.getFirstNameElement(this.firstName));
		}
		if(this.lastName != null){
			tenantBean.setLastname(TenantInfoBeanAttributesFactory.getLastNameElement(this.lastName));
		}
		if(this.tenantDomain != null){
			tenantBean.setTenantDomain(TenantInfoBeanAttributesFactory.getTenantDomainElament(this.tenantDomain));
		}
		if(this.tenantId > 0){
			tenantBean.setTenantId(this.tenantId);
		}
		JAXBElement<TenantInfoBean> tenantInfoElement = factory.createUpdateTenantTenantInfoBean(tenantBean);
		tenantPayLoad.setTenantInfoBean(tenantInfoElement);
		return tenantPayLoad;
	}


	public void setTenantId(int tenantId) {
		this.tenantId = tenantId;
	}
	
	public int getTenantId(){
		return this.tenantId;
	}


	public Object getSystemRepresentation() throws ValidationException {
		return this.getUpdateTenantRequest();
	}
}
