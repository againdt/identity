package com.getinsured.wso2_530.wrapper;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.identity.um.ws.service.ObjectFactory;
import com.getinsured.identity.um.ws.service.UpdateCredentialByAdmin;

//import com.getinsured.iex.im.wso2.carbon.um.ws.service.ObjectFactory;
//import com.getinsured.iex.im.wso2.carbon.um.ws.service.UpdateCredentialByAdmin;

public class UpdateCredentialsByAdminWrapper implements JSONAware {
	private String userName = null;
	private String newCredential = null;
	private static final ObjectFactory factory = new ObjectFactory();
	
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setNewCredential(String newCredential) {
		this.newCredential = newCredential;
	}
	
	public Object getSystemRepresentation(){
		UpdateCredentialByAdmin ucreds = factory.createUpdateCredentialByAdmin();
		QName userQName  = new QName("http://service.ws.um.carbon.wso2.org", "userName");
		QName ncreds = new QName("http://service.ws.um.carbon.wso2.org", "newCredential");
		JAXBElement<String> username = new JAXBElement<String>(userQName, String.class,this.userName);
		JAXBElement<String> new_credentials = new JAXBElement<String>(ncreds, String.class, this.newCredential);
		ucreds.setNewCredential(new_credentials);
		ucreds.setUserName(username);
		return ucreds;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("userName", this.userName);
		obj.put("newCredential", "masked");
		return obj.toJSONString();
	}

}
