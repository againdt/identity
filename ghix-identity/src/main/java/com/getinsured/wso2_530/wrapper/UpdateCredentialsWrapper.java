package com.getinsured.wso2_530.wrapper;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.identity.um.ws.service.ObjectFactory;
import com.getinsured.identity.um.ws.service.UpdateCredential;

//import com.getinsured.iex.im.wso2.carbon.um.ws.service.UpdateCredential;
//import com.getinsured.iex.im.wso2.carbon.um.ws.service.ObjectFactory;

public class UpdateCredentialsWrapper implements JSONAware {
	private String userName = null;
	private String newCredential = null;
	private String oldCredendial = null;
	private static final ObjectFactory factory = new ObjectFactory();
	
	public UpdateCredentialsWrapper(){
		
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setNewCredential(String newCredential) {
		this.newCredential = newCredential;
	}

	public void setOldCredendial(String oldCredendial) {
		this.oldCredendial = oldCredendial;
	}
	
	public UpdateCredential getSystemRepresentation(){
		UpdateCredential ucreds = factory.createUpdateCredential();
		QName userQName  = new QName("http://service.ws.um.carbon.wso2.org", "userName");
		QName ocreds = new QName("http://service.ws.um.carbon.wso2.org", "oldCredential");
		QName ncreds = new QName("http://service.ws.um.carbon.wso2.org", "newCredential");
		JAXBElement<String> username = new JAXBElement<String>(userQName, String.class,this.userName);
		JAXBElement<String> old_credentials = new JAXBElement<String>(ocreds, String.class, this.oldCredendial);
		JAXBElement<String> new_credentials = new JAXBElement<String>(ncreds, String.class, this.newCredential);
		ucreds.setNewCredential(new_credentials);
		ucreds.setOldCredential(old_credentials);
		ucreds.setUserName(username);
		return ucreds;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("userName", this.userName);
		obj.put("oldPassword", "masked");
		obj.put("newCredential", "masked");
		return obj.toJSONString();
	}

}
