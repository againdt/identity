package com.getinsured.wso2_530.wrapper;

import com.getinsured.identity.um.ws.service.AuthenticateResponse;

//import com.getinsured.iex.im.wso2.carbon.um.ws.service.AuthenticateResponse;

public class AuthenticationResponseWrapper {
	private boolean authenticated;
	public AuthenticationResponseWrapper(AuthenticateResponse result){
		this.authenticated = result.isReturn();
	}
	public boolean isAuthenticated() {
		return this.authenticated;
	}
	
}
