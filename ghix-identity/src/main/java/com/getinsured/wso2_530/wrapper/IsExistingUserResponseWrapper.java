package com.getinsured.wso2_530.wrapper;

import com.getinsured.identity.um.ws.service.IsExistingUserResponse;


public class IsExistingUserResponseWrapper {
	private boolean existingUser;
	public IsExistingUserResponseWrapper(IsExistingUserResponse result){
		this.existingUser = result.isReturn();
	}
	public boolean isExistingUser() {
		return this.existingUser;
	}
	
}
