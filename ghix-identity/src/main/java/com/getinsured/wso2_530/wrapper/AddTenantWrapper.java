package com.getinsured.wso2_530.wrapper;

import javax.xml.bind.JAXBElement;

import com.getinsured.gitenant.stratos.common.beans.xsd.TenantInfoBean;
import com.getinsured.gitenant.tenant.mgt.services.AddTenant;
import com.getinsured.gitenant.tenant.mgt.services.ObjectFactory;
import com.getinsured.iex.im.wso2.exceptions.ValidationException;
import com.getinsured.wso2_530.TenantInfoBeanAttributesFactory;


public class AddTenantWrapper {
	
	private static ObjectFactory factory = new ObjectFactory();
	private boolean active;
	private String tenantDomain = null;
	private String adminName = null;
	private String adminPassword = null;
	private String firstName = null;
	private String lastName = null;
	private String adminEmail = null;
	private Integer tenantId;

	
	
	public AddTenantWrapper(){
		
	}


	public String getTenantDomain() {
		return tenantDomain;
	}


	public void setTenantDomain(String tenantDomain) {
		this.tenantDomain = tenantDomain;
	}


	public String getAdminName() {
		return adminName;
	}


	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}


	public String getAdminPassword() {
		return adminPassword;
	}


	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getAdminEmail() {
		return adminEmail;
	}


	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}
	
	
	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}
	

	public Integer getTenantId() {
		return tenantId;
	}


	public void setTenantId(Integer tenantId) {
		this.tenantId = tenantId;
	}


	public AddTenant getAddTenantRequest() throws ValidationException{
		AddTenant tenantPayLoad = factory.createAddTenant();
		TenantInfoBean tenantBean = TenantInfoBeanAttributesFactory.getTenantInfoBean();
		tenantBean.setActive(this.active);
		if(this.adminEmail == null || this.adminName == null){
			throw new ValidationException("Admin credentials are required for adding a tenant");
		}
		if(this.tenantDomain == null){
			throw new ValidationException("Tenant domain is required");
		}
		if(this.firstName == null || this.lastName == null){
			throw new ValidationException("Please check first and last name for tenant admin");
		}
		tenantBean.setAdmin(TenantInfoBeanAttributesFactory.getAdminElement(this.adminName));
		tenantBean.setEmail(TenantInfoBeanAttributesFactory.getEmailElement(this.adminEmail));
		tenantBean.setAdminPassword(TenantInfoBeanAttributesFactory.getAdminPasswordElament(this.adminPassword));
		tenantBean.setFirstname(TenantInfoBeanAttributesFactory.getFirstNameElement(this.firstName));
		tenantBean.setLastname(TenantInfoBeanAttributesFactory.getLastNameElement(this.lastName));
		tenantBean.setTenantDomain(TenantInfoBeanAttributesFactory.getTenantDomainElament(this.tenantDomain));
		JAXBElement<TenantInfoBean> tenantInfoElement = factory.createAddTenantTenantInfoBean(tenantBean);
		tenantPayLoad.setTenantInfoBean(tenantInfoElement);
		return tenantPayLoad;
	}


	public Object getSystemRepresentation() throws ValidationException{
		return this.getAddTenantRequest();
	}
	

}
