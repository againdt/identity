package com.getinsured.wso2_530;

import java.util.HashMap;
import java.util.Map;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class WSO2TenantNamespaceMapper extends NamespacePrefixMapper {

private static final Map<String, String> PREFIXES = new HashMap<String, String>();
	
	static{
		PREFIXES.put("http://services.mgt.tenant.carbon.wso2.org","ser");
		PREFIXES.put("http://echo.services.core.carbon.wso2.org","echo");
		PREFIXES.put("http://beans.common.stratos.carbon.wso2.org/xsd","xsd");
	}
	
	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		
		String prefix = PREFIXES.get(namespaceUri);
		
		if(prefix == null){
			prefix = suggestion;
		}
		return prefix;
	}
}

