package com.getinsured.wso2_530;

public class ClaimValueNotSupported extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ClaimValueNotSupported() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ClaimValueNotSupported(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ClaimValueNotSupported(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ClaimValueNotSupported(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ClaimValueNotSupported(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	

}
