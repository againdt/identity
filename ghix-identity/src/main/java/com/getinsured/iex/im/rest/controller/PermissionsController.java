package com.getinsured.iex.im.rest.controller;

import java.util.HashMap;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.getinsured.identity.cache.UserPermissionProxy;
import com.getinsured.identity.model.RolePermission;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

@Controller
public class PermissionsController {
	
	Logger log = LoggerFactory.getLogger(PermissionsController.class);
	
	@Autowired
	private UserPermissionProxy userPermissionProxy;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/user/permissions/{externalAppUserId}", method = RequestMethod.GET, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> getPermission(@PathVariable String externalAppUserId){
		 try {
			 Set<RolePermission> permissions = null;
			 HashMap<String, Object> userContext = null;
			 userContext = this.userPermissionProxy.getUserPermissionsByExternalAppUserId(externalAppUserId);
			 if(userContext == null){
				 return this.makeResponse(this.makeJsonResponseMessage("No user context available for Id:"+externalAppUserId), HttpStatus.NOT_FOUND);
			 }
			 permissions = (Set<RolePermission>) userContext.get("permissions".intern());
			 if(permissions == null){
				 return this.makeResponse(this.makeJsonResponseMessage("No permissions available for Id:"+externalAppUserId), HttpStatus.NOT_FOUND);
			 }
			 if(log.isDebugEnabled()){
				 log.debug("Received permissions:".intern()+permissions.size());
			 }
			 JSONObject obj = new JSONObject();
			 JSONArray objAray = new JSONArray();
			 for(RolePermission permission: permissions){
				 objAray.add(permission.getPermission().getName());
			 }
			 obj.put("id", externalAppUserId);
			 obj.put("tenantId".intern(), userContext.get("tenantId".intern()));
			 obj.put("mfa_enabled".intern(),userContext.get("mfa_enabled".intern()));
			 obj.put("permissions".intern(), objAray);
			 return this.makeResponse(obj.toJSONString(), HttpStatus.OK);
		} catch (Exception e) {
			HttpStatus status= HttpStatus.INTERNAL_SERVER_ERROR;
			log.error("Error enountered:",e);
			if(e.getMessage().startsWith("Permission Denied".intern())){
				status = HttpStatus.UNAUTHORIZED;
			}
			return this.makeResponse(PlatformServiceUtil.exceptionToJson(e), status);
		}
	}
	
	@SuppressWarnings("unchecked")
	private String makeJsonResponseMessage(String responseStr){
		JSONObject obj = new JSONObject();
		obj.put("error_message", responseStr);
		return obj.toJSONString();
	}
	
	private ResponseEntity<String> makeResponse(String responseStr,
			HttpStatus requestStatus) {
		 HttpHeaders x = new HttpHeaders();
	     x.setContentType(MediaType.valueOf("application/json"));
		 ResponseEntity<String> response = new ResponseEntity<String>(responseStr, x, requestStatus);
		 return response;
				
	}
}
