package com.getinsured.iex.im.wso2.wrappers;

import com.getinsured.iex.im.wso2.carbon.um.ws.service.AuthenticateResponse;

public class AuthenticationResponseWrapper {
	private boolean authenticated;
	public AuthenticationResponseWrapper(AuthenticateResponse result){
		this.authenticated = result.isReturn();
	}
	public boolean isAuthenticated() {
		return this.authenticated;
	}
	
}
