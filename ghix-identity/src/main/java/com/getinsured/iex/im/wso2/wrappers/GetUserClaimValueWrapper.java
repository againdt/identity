package com.getinsured.iex.im.wso2.wrappers;

import java.util.HashSet;
import java.util.Iterator;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.im.wso2.carbon.um.ws.service.GetUserClaimValue;
import com.getinsured.iex.im.wso2.carbon.um.ws.service.ObjectFactory;
import com.getinsured.iex.im.wso2.exceptions.CalimValueNotSupported;

public class GetUserClaimValueWrapper implements JSONAware {
	public static final String SECURITY_QUESTION_1_URI="http://wso2.org/claims/secquestion1";
	public static final String SECURITY_QUESTION_2_URI="http://wso2.org/claims/secquestion2";
	public static final String SECURITY_QUESTION_ANS_1_URI = "http://wso2.org/claims/secanswer1";
	public static final String SECURITY_QUESTION_ANS_2_URI = "http://wso2.org/claims/secanswer2";
	public static final String PREF_COMMUNICATION_METHOD_URI = "http://wso2.org/claims/prefMethComm";
	public static final String RIDP_FLAG_URI="http://wso2.org/claims/ridpflag";
	public static final String USERNAME="http://wso2.org/claims/userName";
	public static final String PASSWORD_TIMESTAMP="http://wso2.org/claims/passwordTimestamp";
	public static final String ACCOUNT_TYPE_CODE = "http://wso2.org/claims/accountTypeCode";
	
	private static HashSet<String> supportedClaims= new HashSet<>();
	private static final QName userQName  = new QName("http://service.ws.um.carbon.wso2.org", "userName");
	private static final QName profileQName = new QName("http://service.ws.um.carbon.wso2.org", "profileName");
	private static final QName claimUriQName = new QName("http://service.ws.um.carbon.wso2.org", "claimURI");
	private static final QName claimValueQName = new QName("http://service.ws.um.carbon.wso2.org", "claimValue");
	
	static{
		supportedClaims.add(PREF_COMMUNICATION_METHOD_URI);
		supportedClaims.add(SECURITY_QUESTION_1_URI);
		supportedClaims.add(SECURITY_QUESTION_ANS_1_URI);
		supportedClaims.add(SECURITY_QUESTION_2_URI);
		supportedClaims.add(SECURITY_QUESTION_ANS_2_URI);
		supportedClaims.add(RIDP_FLAG_URI);
		supportedClaims.add(USERNAME);
		supportedClaims.add(PASSWORD_TIMESTAMP);
		supportedClaims.add(ACCOUNT_TYPE_CODE);
	}
	
	private static final ObjectFactory factory = new ObjectFactory();
	private String userName;
	private String profileName;
	private GetUserClaimValue getClaimReq;
	private String claim;
		
	public GetUserClaimValueWrapper(){
		getClaimReq = factory.createGetUserClaimValue();
	}
	
	public void setTargetUser(String userName, String profileName){
		this.userName = userName;
		this.profileName = profileName;
		this.getClaimReq.setUserName(new JAXBElement<String>(userQName, String.class,this.userName));
		this.getClaimReq.setProfileName(new JAXBElement<String>(profileQName, String.class,this.profileName));
	}
	
	public void setUserClaim(String claimUri) throws CalimValueNotSupported{
		if(claimUri == null ){
			return;
		}

		boolean isClaimPresent = false;
		String supportedClaim = null;
		
		if(null != supportedClaims){
			Iterator<String> iterator = supportedClaims.iterator();
			while(iterator.hasNext()) {
				supportedClaim = iterator.next();
				if(supportedClaim.equalsIgnoreCase(claimUri)) {
					isClaimPresent = true;
					break;
				} 
			}
		}
		
		if(!isClaimPresent) {
			throw new CalimValueNotSupported(claimUri);
		} 
		
		this.claim = supportedClaim;
		
		this.getClaimReq.setClaim(new JAXBElement<String>(claimUriQName, String.class,supportedClaim));
		this.getClaimReq.setProfileName(new JAXBElement<String>(claimValueQName, String.class,profileName));
	}
	
	public Object getSystemRepresentation(){
		return this.getClaimReq;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("USER_NAME", this.userName);
		obj.put("CLAIM_URI", this.claim);
		return obj.toJSONString();
	}

}
