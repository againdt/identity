package com.getinsured.iex.im.wso2.wrappers;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import com.getinsured.iex.im.wso2.carbon.um.ws.service.Authenticate;
import com.getinsured.iex.im.wso2.carbon.um.ws.service.ObjectFactory;

public class AuthenticateWrapper {
	private String userName = null;
	private String credential = null;
	private static ObjectFactory factory = null;
	
	public AuthenticateWrapper(){
		if(factory == null){
			factory = new ObjectFactory();
		}
		
	}
	
	public Authenticate getSystemRepresentation(){
		Authenticate auth = factory.createAuthenticate();
		QName userQName  = new QName("http://service.ws.um.carbon.wso2.org", "userName");
		QName creds = new QName("http://service.ws.um.carbon.wso2.org", "credential");
		JAXBElement<String> username = new JAXBElement<String>(userQName, String.class,this.userName);
		JAXBElement<String> credentials = new JAXBElement<String>(creds, String.class, this.credential);
		auth.setCredential(credentials);
		auth.setUserName(username);
		return auth;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCredential() {
		return credential;
	}

	public void setCredential(String credential) {
		this.credential = credential;
	}
}
