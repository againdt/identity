package com.getinsured.iex.im.wso2.wrappers;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.im.wso2.carbon.um.ws.service.IsExistingUser;
import com.getinsured.iex.im.wso2.carbon.um.ws.service.ObjectFactory;

public class IsExistingUserWrapper implements JSONAware {
	private String userName = null;
	private static final ObjectFactory factory = new ObjectFactory();
	
	public IsExistingUserWrapper(){
		
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
	
	public IsExistingUser getSystemRepresentation(){
		IsExistingUser isExistingUser = factory.createIsExistingUser();
		QName userQName  = new QName("http://service.ws.um.carbon.wso2.org", "userName");
		JAXBElement<String> username = new JAXBElement<String>(userQName, String.class,this.userName);
		isExistingUser.setUserName(username);
		return isExistingUser;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("userName", this.userName);
		return obj.toJSONString();
	}

}
