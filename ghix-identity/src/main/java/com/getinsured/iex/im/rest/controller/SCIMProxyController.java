package com.getinsured.iex.im.rest.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.wso2.charon.core.schema.SCIMConstants;

import com.getinsured.hix.platform.util.Utils;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.security.HubSecureHttpClient;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

@Controller
public class SCIMProxyController {
	private static String errResponseJson = "{\"BAD REQUEST\":\"Null Or Empty For Operation- %s\"}";
	
	public static String SCIM_USER_ENDPOINT;
	
	private static String SCIM_ADMIN_USER;

	private static String SCIM_ADMIN_PASS;

	private CloseableHttpClient httpClient;

	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(IdentityManagerController.class);

	@RequestMapping(value = "/createUser", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> createUser(
			@RequestBody String createUserPayload) {
		if (createUserPayload == null || createUserPayload.length() == 0) {
			return this.makeResponse(
					String.format(errResponseJson, "Create user"),
					HttpStatus.BAD_REQUEST, null);
		}
		return this.postProxy(createUserPayload, "USER CREATION ");
	}

	@RequestMapping(value = "/findUser", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> findUser(@RequestParam(value="filter") String filter) {
		if (filter == null || filter.length() == 0) {
			return this.makeResponse(
					String.format(errResponseJson, "Find User"),
					HttpStatus.BAD_REQUEST, null);
		}
		return this.getProxy(filter, "FIND USER  ");
	}

	@RequestMapping(value = "/updateUserAdditionalAttribs/{scimId}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> updateUserAdditionalAttribs(
			@PathVariable("scimId") String scimId,@RequestBody String payload) {
		if (scimId == null || scimId.length() == 0 || payload == null || payload.length() == 0) {
			return this.makeResponse(String.format(errResponseJson,
					"Update Additional User Attrib"), HttpStatus.BAD_REQUEST,
					null);
		}
		return this.putProxy(scimId,payload, "UPDATE ADDITIONAL USER ATTRIBUTES ");
	}

	@RequestMapping(value = "/updateUser/{scimId}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> updateUser(@PathVariable("scimId") String scimId, @RequestBody String payload) {
		if (scimId == null || scimId.length() == 0 || payload == null || payload.length() == 0) {
			return this.makeResponse(
					String.format(errResponseJson, "Update User"),
					HttpStatus.BAD_REQUEST, null);
		}
		return this.putProxy(scimId, payload, "UPDATE USER ");
	}

	public ResponseEntity<String> postProxy(String payload, String operation) {
		HttpEntity requestEntity = null;
		HttpPost httpost = null;
		CloseableHttpResponse httpResponse = null;
		HttpEntity responseEntity = null;
		int responseStatus = 500;
		Exception ex = null;
		String authHeader = null;
		
		try {
			if (this.httpClient == null) {
				httpClient = HubSecureHttpClient.getBasicAuthHttpClient();
			}
			requestEntity = EntityBuilder.create()
					.setContentType(ContentType.APPLICATION_JSON)
					.setText(payload).build();
			httpost = new HttpPost(SCIM_USER_ENDPOINT);
			
			authHeader = Utils.getAuthorizationHeader(false, null, SCIM_ADMIN_USER, SCIM_ADMIN_PASS);
			httpost.setHeader(SCIMConstants.AUTHORIZATION_HEADER, authHeader);
			
			httpost.setEntity(requestEntity);
			httpResponse = httpClient.execute(httpost);
			
			BufferedReader br = new BufferedReader(
                    new InputStreamReader((httpResponse.getEntity().getContent())));

			String output;
			StringBuffer sb = new StringBuffer();
			while ((output = br.readLine()) != null) {
				//LOGGER.info(output);
				sb.append(output);
			}
			br.close();
			responseEntity = httpResponse.getEntity();
			responseStatus = httpResponse.getStatusLine().getStatusCode();
		/*	BufferedInputStream bis = new BufferedInputStream(
					responseEntity.getContent());
			byte[] buf = new byte[bis.available()];
			bis.read(buf);
			LOGGER.info(operation + httpResponse.getStatusLine() + " buf: " + buf);*/
			ResponseEntity<String> response =  this.makeResponse(sb.toString(),
					HttpStatus.valueOf(responseStatus),
					responseEntity.getContentType());
			return response;
			
		} catch (IOException | HubServiceException e) {
			ex = e;
		} finally {
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					LOGGER.error("Error closing response [" + e.getMessage()
							+ " Ignoring]", e);
				}
			}
		}
		return this.makeResponse(PlatformServiceUtil.exceptionToJson(ex),
				HttpStatus.INTERNAL_SERVER_ERROR, null);
	}

	public ResponseEntity<String> getProxy(String queryStr, String operation) {
		HttpGet httpGet = null;
		CloseableHttpResponse httpResponse = null;
		HttpEntity responseEntity = null;
		int responseStatus = 500;
		Exception ex = null;
		String authHeader = null;
		
		try {
			if (this.httpClient == null) {
				httpClient = HubSecureHttpClient.getBasicAuthHttpClient();
			}
			httpGet = new HttpGet(SCIM_USER_ENDPOINT
					+ "?filter=" + queryStr);
			
			authHeader = Utils.getAuthorizationHeader(false, null, SCIM_ADMIN_USER, SCIM_ADMIN_PASS);
			httpGet.setHeader(SCIMConstants.AUTHORIZATION_HEADER, authHeader);
			
			httpResponse = httpClient.execute(httpGet);
			responseEntity = httpResponse.getEntity();
			responseStatus = httpResponse.getStatusLine().getStatusCode();
			BufferedReader br= new BufferedReader(new InputStreamReader(responseEntity.getContent()));
			StringBuilder sb = new StringBuilder();
			String str = null;
			while((str = br.readLine()) != null) {
				sb.append(str);
			}
			sb.trimToSize();
			br.close();
			//LOGGER.info(operation + httpResponse.getStatusLine());
			return this.makeResponse(sb.toString(),
					HttpStatus.valueOf(responseStatus),
					responseEntity.getContentType());
		} catch (IOException | HubServiceException e) {
			ex = e;
		} finally {
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					LOGGER.error("Error closing response [" + e.getMessage()
							+ " Ignoring]", e);
				}
			}
		}
		return this.makeResponse(PlatformServiceUtil.exceptionToJson(ex),
				HttpStatus.INTERNAL_SERVER_ERROR, null);
	}

	public ResponseEntity<String> putProxy(String scimId, String payLoad, String operation) {
		HttpPut httpPut = null;
		CloseableHttpResponse httpResponse = null;
		HttpEntity responseEntity = null;
		int responseStatus = 500;
		Exception ex = null;
		String authHeader = null; 
		
		try {
			if (this.httpClient == null) {
				httpClient = HubSecureHttpClient.getBasicAuthHttpClient();
			}
			httpPut = new HttpPut(SCIM_USER_ENDPOINT+"/"+scimId);
			HttpEntity requestEntity = EntityBuilder.create()
					.setContentType(ContentType.APPLICATION_JSON)
					.setText(payLoad).build();
			
			authHeader = Utils.getAuthorizationHeader(false, null, SCIM_ADMIN_USER, SCIM_ADMIN_PASS);
			httpPut.setHeader(SCIMConstants.AUTHORIZATION_HEADER, authHeader);
			
			httpPut.setEntity(requestEntity);
			httpResponse = httpClient.execute(httpPut);
			responseEntity = httpResponse.getEntity();
			responseStatus = httpResponse.getStatusLine().getStatusCode();
			BufferedReader br= new BufferedReader(new InputStreamReader(responseEntity.getContent()));
			StringBuilder sb = new StringBuilder();
			String str = null;
			while((str = br.readLine()) != null) {
				sb.append(str);
			}
			sb.trimToSize();
			br.close();
			//LOGGER.info(operation + httpResponse.getStatusLine());
			return this.makeResponse(sb.toString(),
					HttpStatus.valueOf(responseStatus),
					responseEntity.getContentType());
		} catch (IOException | HubServiceException e) {
			ex = e;
		} finally {
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					LOGGER.error("Error closing response [" + e.getMessage()
							+ " Ignoring]", e);
				}
			}
		}
		return this.makeResponse(PlatformServiceUtil.exceptionToJson(ex),
				HttpStatus.INTERNAL_SERVER_ERROR, null);
	}

	private ResponseEntity<String> makeResponse(String responseStr,
			HttpStatus requestStatus, Header header) {
		HttpHeaders x = new HttpHeaders();
		if (header != null) {
			x.setContentType(MediaType.valueOf(header.getValue()));
		} else {
			x.setContentType(MediaType.APPLICATION_JSON);
		}
		x.setContentLength(responseStr.length());
		ResponseEntity<String> response = new ResponseEntity<String>(
				responseStr, x, requestStatus);
		return response;
	}
	
	@Value("#{configProp['scim.user.endpoint']}")
	public void setSCIMUserEndpoint(String userEndpoint){
		SCIM_USER_ENDPOINT = userEndpoint;
	}
	
	@Value("#{configProp['scim.provisioning.user']}")
	public void setSCIMUser(String scimUserName){
		SCIM_ADMIN_USER = scimUserName;
	}
	
	@Value("#{configProp['scim.provisioning.password']}")
	public void setSCIMUserPassword(String scimPass){
		SCIM_ADMIN_PASS = scimPass;
	}
}
