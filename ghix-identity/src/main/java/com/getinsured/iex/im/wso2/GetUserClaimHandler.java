package com.getinsured.iex.im.wso2;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import com.getinsured.identity.um.ws.service.GetUserClaimValueResponse;
import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.im.wso2.exceptions.CalimValueNotSupported;
import com.getinsured.iex.im.wso2.wrappers.GetUserClaimValueWrapper;
public class GetUserClaimHandler extends AbstractServiceHandler {

	private boolean handleResponseCalled = false;
	private GetUserClaimValueResponse response = null;

	@Override
	public Object getRequestpayLoad() throws HubServiceException,
			HubMappingException {
		JSONParser parser = new JSONParser();
		try {
			JSONObject updateUserJsonrequest = (JSONObject) parser.parse(this.getJsonInput());
			String userName = (String) updateUserJsonrequest.get("userName");
			String claimUri = (String) updateUserJsonrequest.get("claim");
			String profileName = (String) updateUserJsonrequest.get("profileName");
			GetUserClaimValueWrapper wrapper = new GetUserClaimValueWrapper();
			wrapper.setTargetUser(userName,profileName);
			wrapper.setUserClaim(claimUri);
			return wrapper.getSystemRepresentation();
		} catch (ParseException | CalimValueNotSupported e) {
			throw new HubServiceException("Failed to construct the payload from request input",e);
		}
	}

	@Override
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		if(responseObject == null){
			throw new HubServiceException("Handle response called with null response object");
		}
		try{
			response = (GetUserClaimValueResponse)responseObject;
		}catch(ClassCastException ce){
			throw new HubServiceException("Expected GetUserClaimValueResponse response, received "+response.getClass().getName());
		}
		return response.getReturn().getValue();
	}

	@Override
	public RequestStatus getRequestStatus() {
		if(this.handleResponseCalled){
			return RequestStatus.SUCCESS;
		}
		return RequestStatus.FAILED;
	}

	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Marshaller getServiceMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

}
