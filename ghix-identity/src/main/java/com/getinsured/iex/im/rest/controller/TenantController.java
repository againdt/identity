package com.getinsured.iex.im.rest.controller;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.gitenant.tenant.mgt.services.TenantAwareWsTemplateFactory;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

@Controller
public class TenantController extends MessageProcessor {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(TenantController.class);
	
	@Autowired
	private TenantAwareWsTemplateFactory tenantAwareTemplateFactory;
	
	@RequestMapping(value = "/tenant/add", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> addTenant(@RequestBody String userNamePayload){
	
		ResponseEntity<String> response = null;
		try{
			JSONParser parser = new JSONParser();
			JSONObject objInputJson = null;
			objInputJson = (JSONObject)parser.parse(userNamePayload);
			String inputJSON = ((JSONObject) objInputJson.get("payload")).toJSONString();
			
			response = addTenant((String) objInputJson.get("clientIp"), inputJSON);
			
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
	
	@RequestMapping(value = "/tenant/update", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> updateTenant(@RequestBody String userNamePayload){
	
		ResponseEntity<String> response = null;
		try{
			JSONParser parser = new JSONParser();
			JSONObject objInputJson = null;
			objInputJson = (JSONObject)parser.parse(userNamePayload);
			String inputJSON = ((JSONObject) objInputJson.get("payload")).toJSONString();
			
			response = updateTenant((String) objInputJson.get("clientIp"), inputJSON);
			
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}

	private ResponseEntity<String> addTenant(String clientIp, String inputJSON) {
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge authBridge = HubServiceBridge.getHubServiceBridge("addTenantRequest");
			handler = authBridge.getServiceHandler();
			
			handler.setJsonInput(inputJSON);
			
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("WSO2_TENANT");
			requestRecord.setEndpointOperationName("WSO2_ADD_TENANT");
			requestRecord.setAccessIp(clientIp);
			requestRecord.setStatus("PENDING");
			response = this.executeRequest(requestRecord, handler, tenantAwareTemplateFactory.getTenantServiceTemplate());
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
	
	private ResponseEntity<String> updateTenant(String clientIp, String inputJSON) {
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge authBridge = HubServiceBridge.getHubServiceBridge("updateTenantRequest");
			handler = authBridge.getServiceHandler();
			
			handler.setJsonInput(inputJSON);
			
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("WSO2_TENANT");
			requestRecord.setEndpointOperationName("WSO2_UPDATE_TENANT");
			requestRecord.setAccessIp(clientIp);
			requestRecord.setStatus("PENDING");
			response = this.executeRequest(requestRecord, handler, tenantAwareTemplateFactory.getTenantServiceTemplate());
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
}
