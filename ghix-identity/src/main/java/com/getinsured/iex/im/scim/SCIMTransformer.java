package com.getinsured.iex.im.scim;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wso2.charon3.core.attributes.Attribute;
import org.wso2.charon3.core.attributes.ComplexAttribute;
import org.wso2.charon3.core.attributes.DefaultAttributeFactory;
import org.wso2.charon3.core.attributes.SimpleAttribute;
import org.wso2.charon3.core.exceptions.BadRequestException;
import org.wso2.charon3.core.exceptions.CharonException;
import org.wso2.charon3.core.objects.User;
import org.wso2.charon3.core.schema.AttributeSchema;
import org.wso2.charon3.core.schema.SCIMSchemaDefinitions;

import com.getinsured.hix.platform.util.SCIMClientConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.hub.platform.ValidationException;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

public class SCIMTransformer {
	

	private static final Logger LOGGER = LoggerFactory.getLogger(SCIMTransformer.class);
	
	private SimpleAttribute getSimpleAttribute(AttributeSchema attributeSchema, String attributeName, Object value){
		SimpleAttribute simpleAttribute = new SimpleAttribute(attributeName, value);
		
		try {
			simpleAttribute = (SimpleAttribute) DefaultAttributeFactory.createAttribute(attributeSchema, simpleAttribute);
			return simpleAttribute;
		} catch (CharonException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LOGGER.error("Failed to create atribute for:"+attributeName+" Value:"+value);
		return null;
	}
	
	public User getScimUserForUpdate(JSONObject userDataObj) throws CharonException, ValidationException, BadRequestException {
		JSONArray errors = null;
		User user = new User();
		SCIMTransformer scimTransformer = new SCIMTransformer();
		if(errors == null){
			errors = new JSONArray();
		}
		scimTransformer.extractUserDataForUpdate(user, userDataObj, errors);
		Attribute wso2ExtensionAttribute = scimTransformer
				.createWSO2ExtensionAttributeForUpdate(userDataObj,errors);
		if(errors.size() > 0){
			ValidationException ve = new ValidationException();
			ve.setErrors(errors);
			throw ve;
		}
		user.setAttribute(wso2ExtensionAttribute);
		user.setSchema("urn:scim:schemas:core:1.0");
		user.setSchema("urn:scim:schemas:extension:wso2:1.0");
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public User getScimUser(JSONObject userDataObj, HashSet<String> requiredAttributes) throws CharonException, ValidationException {
		JSONArray errors = null;
		User user = new User();
		boolean validationEnabled = true;;
		if(requiredAttributes == null || requiredAttributes.size() == 0){
			validationEnabled = false;
		}
		String tmp = (String) userDataObj.get(SCIMClientConstants.USER_NAME_ATTR);
		if(tmp == null){
			if(errors == null){
				errors = new JSONArray();
			}
			errors.add("No user name available".intern());
		}else{
			try {
				user.setUserName(tmp);
			} catch (BadRequestException e) {
				if(errors == null){
					errors = new JSONArray();
				}
				errors.add("System Error:"+e.getMessage());
			}
		}
		tmp = (String) userDataObj.get(SCIMClientConstants.ID_ATTR);
		if(tmp == null){
			if(errors == null){
				errors = new JSONArray();
			}
			if(validationEnabled && requiredAttributes.contains(SCIMClientConstants.ID_ATTR)){
				errors.add("No user id available".intern());
			}
		}else{
			try {
				user.setId(tmp);
			} catch (BadRequestException e) {
				if(errors == null){
					errors = new JSONArray();
				}
				errors.add("System Error:"+e.getMessage());
			}
		}
		tmp = (String) userDataObj.get("userPassword".intern());
		if(tmp == null){
			if(errors == null){
				errors = new JSONArray();
			}
			if(validationEnabled && requiredAttributes.contains("userPassword".intern())){
				errors.add("No user password available".intern());
			}
		}else{
			SimpleAttribute attr = this.getSimpleAttribute(SCIMSchemaDefinitions.SCIMUserSchemaDefinition.PASSWORD, "password", (String)tmp);
			user.setAttribute(attr);
		}
		SCIMTransformer scimTransformer = new SCIMTransformer();
		if(errors == null){
			errors = new JSONArray();
		}
		scimTransformer.extractUserData(user, userDataObj, errors);
		Attribute wso2ExtensionAttribute = scimTransformer
				.createWSO2ExtensionAttribute(userDataObj,errors);
		if(errors.size() > 0){
			ValidationException ve = new ValidationException();
			ve.setErrors(errors);
			throw ve;
		}
		user.setAttribute(wso2ExtensionAttribute);
		user.setSchema("urn:scim:schemas:core:1.0");
		user.setSchema("urn:scim:schemas:extension:wso2:1.0");
		//user.setSchemas();
		return user;
	}

	@SuppressWarnings("unchecked")
	public void extractUserData(User user,JSONObject userData, JSONArray errors){
			String firstName = null;
			String lastName = null;
			Object tmp = userData.get("firstName"); //Mandatory
			if(tmp == null){
				errors.add("Mandatory field first name not available");
			}else{
				firstName = (String)tmp;
			}
			tmp = userData.get("lastName"); // Mandatory
			if(tmp == null){
				errors.add("Mandatory field last name not available");
			}else{
				lastName = (String)tmp;
			}
			ComplexAttribute name = this.getNameAttribute(firstName, lastName, errors);
			user.setAttribute(name);
			tmp = userData.get("preferredLanguage"); //Optional
			if(tmp != null){
				SimpleAttribute attr = this.getSimpleAttribute(SCIMSchemaDefinitions.SCIMUserSchemaDefinition.PREFERRED_LANGUAGE, "preferredLanguage", tmp);
				user.setAttribute(attr);
			}
			tmp = userData.get("userType"); // Mandatory
			if(tmp == null){
				errors.add("Mandatory field userType not available");
			}else{
				SimpleAttribute attr = this.getSimpleAttribute(SCIMSchemaDefinitions.SCIMUserSchemaDefinition.USER_TYPE, "userType", Long.toString((Long)tmp));
				user.setAttribute(attr);
			}
			tmp = userData.get("mail"); //Mandatory
			if(tmp == null){
				errors.add("Mandatory field email not available");
			}else{
				SimpleAttribute attr = this.getSimpleAttribute(SCIMSchemaDefinitions.SCIMUserSchemaDefinition.EMAILS, "emails", tmp);
				user.setAttribute(attr);
			}
	}
	
	public void extractUserDataForUpdate(User user,JSONObject userData, JSONArray errors) throws CharonException, BadRequestException{
			String firstName = null;
			String lastName = null;
			String userName = null;
			Object tmp = userData.get("firstName"); //Optional
			if(tmp != null){
				firstName = (String)tmp;
			}
			tmp = userData.get("lastName"); //Optional
			if(tmp != null){
				lastName = (String)tmp;
			}
			if(firstName != null || lastName != null){
				ComplexAttribute name = this.getNameAttribute(firstName, lastName, errors);
				user.setAttribute(name);
			}
			tmp = userData.get("userName"); //Optional
			if(tmp != null){
				userName = (String)tmp;
				user.setUserName(userName);
			}
			tmp = userData.get("preferredLanguage"); //Optional
			if(tmp != null){
				SimpleAttribute attr = this.getSimpleAttribute(SCIMSchemaDefinitions.SCIMUserSchemaDefinition.PREFERRED_LANGUAGE, "preferredLanguage", tmp);
				user.setAttribute(attr);
			}
			tmp = userData.get("userType"); // Mandatory
			if(tmp != null){
				LOGGER.warn("Received userType value, not allowed to be upated, Ignoring".intern());
			}
			tmp = userData.get("mail"); 
			if(tmp != null){
				SimpleAttribute attr = this.getSimpleAttribute(SCIMSchemaDefinitions.SCIMUserSchemaDefinition.EMAILS, "emails", tmp);
				user.setAttribute(attr);
			}
	}
	
	private ComplexAttribute getNameAttribute(String firstName, String lastName, JSONArray errors){
		ComplexAttribute wso2NameAttribute = new ComplexAttribute("name");
		Map<String, Attribute> attrMap = null;
		attrMap = new HashMap<String, Attribute>();
		createAndAddAttribute(attrMap, "givenName".intern(),firstName,false, errors);
		createAndAddAttribute(attrMap, "familyName".intern(),lastName,false, errors);
		wso2NameAttribute.setSubAttributesList(attrMap);
		return wso2NameAttribute;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getUserDataFromSCIMResponse(JSONObject scimResponse){
		System.out.println(scimResponse.toJSONString());
		JSONObject returnObj = new JSONObject();
		String userName = (String)scimResponse.get("userName".intern());//root
		returnObj.put("userName".intern(), userName);
		
		String id = (String)scimResponse.get("id".intern());//root
		returnObj.put("id".intern(), id);
		JSONObject meta = (JSONObject)scimResponse.get("meta".intern());//root
		if(meta != null){
			String created = (String)meta.get("created".intern());
			returnObj.put("created".intern(), created);
			
			String location = (String)meta.get("location".intern());
			returnObj.put("location".intern(), location);
			
			String lastModified = (String)meta.get("lastModified".intern());
			returnObj.put("last_modified".intern(), lastModified);
		}
		return returnObj;
	}
	/**
	 * Method to create WSO2Extension element
	 * @param errors 
	 * 
	 * @param accountUser
	 *            Object containing GHIX user attributes
	 * @param attrMap
	 *            Map to populate SCIM WSO2 Extension attributes with
	 * @return ComplexAttribute representing the WSO2Extension element or NULL if there is any exception
	 */
	public ComplexAttribute createWSO2ExtensionAttribute(JSONObject userData, JSONArray errors) {
		ComplexAttribute wso2ExtensionAttribute = null;
		Map<String, Attribute> attrMap = null;
			attrMap = new HashMap<String, Attribute>();
			Object tmp = null; 
			tmp = userData.get("tenantCode".intern().intern()); 
			createAndAddAttribute(attrMap, "giTenantId".intern().intern(),(String)tmp,true, errors); 						//Mandatory
			tmp = userData.get(SCIMClientConstants.PHONE_NUMBER_ATTR); 
			createAndAddAttribute(attrMap, SCIMClientConstants.PHONE_NUMBER_ATTR,(String)tmp,true, errors); 				//Mandatory
			tmp = userData.get(SCIMClientConstants.PREF_METH_COMM_ATTR); 
			createAndAddAttribute(attrMap, SCIMClientConstants.PREF_METH_COMM_ATTR,(String)tmp,false, errors); 				//Optional
			tmp = userData.get("secQuestion1".intern()); 
			createAndAddAttribute(attrMap,"secQuestion1".intern(),(String)tmp,false, errors); 								//Optional
			tmp = userData.get("secAnswer1".intern()); 
			
			String hashAnswer = (String)userData.get("hashAnswer1");
			
			if(tmp != null && (hashAnswer == null || hashAnswer.equalsIgnoreCase("Y".intern()))){
				hashAnswer = PlatformServiceUtil.getHash(((String)tmp).toLowerCase());
			}else {
				hashAnswer = (String)tmp;
			}
			
			createAndAddAttribute(attrMap, "secAnswer1".intern(),hashAnswer,false, errors); 	//Optional
			tmp = userData.get("secQuestion2".intern()); 
			createAndAddAttribute(attrMap,"secQuestion2".intern(),(String)tmp,false, errors);								//Optional
			tmp = userData.get("secAnswer2".intern()); 
			hashAnswer = (String)userData.get("hashAnswer2");
			
			if(tmp != null && (hashAnswer == null || hashAnswer.equalsIgnoreCase("Y".intern()))){
				hashAnswer = PlatformServiceUtil.getHash(((String)tmp).toLowerCase());
			}else {
				hashAnswer = (String)tmp;
			}
			createAndAddAttribute(attrMap, "secAnswer2".intern(),hashAnswer,false, errors); 	//Optional
			wso2ExtensionAttribute = new ComplexAttribute(SCIMClientConstants.WSO2_EXTENSION_ATTR);
			wso2ExtensionAttribute.setSubAttributesList(attrMap);
		return wso2ExtensionAttribute;
	}
	
	/**
	 * Reads and creates update allowed attributes only, No updates allowed for tenant code 
	 * @param userData
	 * @param errors
	 * @return
	 */
	public ComplexAttribute createWSO2ExtensionAttributeForUpdate(JSONObject userData, JSONArray errors) {
		ComplexAttribute wso2ExtensionAttribute = null;
		Map<String, Attribute> attrMap = new HashMap<String, Attribute>();
			Object tmp = null; 
			tmp = userData.get(SCIMClientConstants.PHONE_NUMBER_ATTR); 
			createAndAddAttribute(attrMap, SCIMClientConstants.PHONE_NUMBER_ATTR,(String)tmp,false, errors); 				//Mandatory
			tmp = userData.get(SCIMClientConstants.PREF_METH_COMM_ATTR); 
			createAndAddAttribute(attrMap, SCIMClientConstants.PREF_METH_COMM_ATTR,(String)tmp,false, errors); 				//Optional
			tmp = userData.get("secQuestion1".intern()); 
			createAndAddAttribute(attrMap,"secQuestion1".intern(),(String)tmp,false, errors); 								//Optional
			tmp = userData.get("secAnswer1".intern()); 
			
			String hashAnswer = (String)userData.get("hashAnswer1");
			
			if(tmp != null && (hashAnswer == null || hashAnswer.equalsIgnoreCase("Y".intern()))){
				hashAnswer = PlatformServiceUtil.getHash(((String)tmp).toLowerCase());
			}else {
				hashAnswer = (String)tmp;
			}
			createAndAddAttribute(attrMap, "secAnswer1".intern(),hashAnswer,false, errors); 	//Optional
			tmp = userData.get("secQuestion2".intern()); 
			createAndAddAttribute(attrMap,"secQuestion2".intern(),(String)tmp,false, errors);								//Optional
			tmp = userData.get("secAnswer2".intern()); 
			
			hashAnswer = (String)userData.get("hashAnswer2");
			
			if(tmp != null && (hashAnswer == null || hashAnswer.equalsIgnoreCase("Y".intern()))){
				hashAnswer = PlatformServiceUtil.getHash(((String)tmp).toLowerCase());
			}else {
				hashAnswer = (String)tmp;
			}
			createAndAddAttribute(attrMap, "secAnswer2".intern(),hashAnswer,false, errors); 	//Optional
			tmp = userData.get("tenantCode".intern()); 
			createAndAddAttribute(attrMap, "giTenantId".intern(),(String)tmp,false, errors); 	//Optional
						
			wso2ExtensionAttribute = new ComplexAttribute(SCIMClientConstants.WSO2_EXTENSION_ATTR);
			wso2ExtensionAttribute.setSubAttributesList(attrMap);
			return wso2ExtensionAttribute;
	}
	
	
	
	/**
	 *  Method to create WSO2Extension element
	 * @param dob String containing date of birth
	 * @param ssn String containing social security number
	 * @param preferredLanguage String containinig preferred language
	 * @param ridpFlag String containing ridp flag
	 * @return ComplexAttribute representing the WSO2Extension element
	 * @throws GIException
	 */
	public ComplexAttribute addWSO2ExtensionAdditonalAttributes(String dob, String ssn, String preferredLanguage, String ridpFlag,JSONArray errors)
			throws GIException {
		ComplexAttribute wso2ExtensionAttribute = null;
		GIException ex = null;
		Map<String, Attribute> attrMap = null;
		try {
			attrMap = new HashMap<String, Attribute>();
			createAndAddAttribute(attrMap, SCIMClientConstants.DOB_ATTR,dob,false, errors); 								//Optional
			createAndAddAttribute(attrMap, SCIMClientConstants.SSN_ATTR, ssn,false, errors); 								//Optional
			createAndAddAttribute(attrMap,SCIMClientConstants.PREFERRED_LANGUAGE_ATTR, preferredLanguage,false, errors); 	//Optional
			createAndAddAttribute(attrMap, SCIMClientConstants.RIDP_FLAG_ATTR, ridpFlag,false, errors); 					// optional
			wso2ExtensionAttribute = new ComplexAttribute(SCIMClientConstants.WSO2_EXTENSION_ATTR);
			//Its possible we have 0 attributes available, app extended attributes are optional, 
			//go ahead and add the empty map if that's the case, 
			wso2ExtensionAttribute.setSubAttributesList(attrMap);
		} catch (Exception e) {
			LOGGER.error("ERR WHILE SETTING WSO2 EXTENSION ELEMENTS: ", e);
			ex = new GIException(e.getMessage(), e);
		}
		if (null != ex) {
			throw ex;
		}

		return wso2ExtensionAttribute;
	}

	/**
	 * Method to set simple attributes in attribute map
	 * 
	 * @param attrMap
	 *            Map of attributes stored in key,attribute format
	 * @param attrName
	 *            Name of the attribute
	 * @param attrValue
	 *            Attribute value
	 */
	@SuppressWarnings("unchecked")
	public void createAndAddAttribute(Map<String, Attribute> attrMap,
			String attrName, String attrValue, boolean mandatory, JSONArray errors) {
		Attribute attribute = null;
		if((attrValue == null || attrValue.length() == 0)){
			if(mandatory){
				errors.add("Attribute:"+attrName+" is required");
			}
			return;
		}
		if (null != attrName && attrName.length() > 0) {
			attribute = new SimpleAttribute(attrName, attrValue);
			if (null != attribute) {
				attrMap.put(attrName, attribute);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void addAttributeIfNotNull(String attr, Object val, JSONObject obj){
		if(val != null){
			if(LOGGER.isTraceEnabled()){
				LOGGER.trace("Adding attribute \""+attr+"\" with value \""+val+"\"");
			}
			obj.put(attr, val);		
		}else{
			if(LOGGER.isTraceEnabled()){
				LOGGER.trace("Skipping attribute \""+attr+"\" No value received");
			}
		}
	}

	@SuppressWarnings("unchecked")
	public JSONObject getUserDetailsFromSCIMResponse(JSONObject scimResponse) {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Processing SCIMJSON Response:"+scimResponse.toJSONString());
		}
		JSONObject returnObj = new JSONObject();
		String userName = (String)scimResponse.get("userName".intern());//root
		addAttributeIfNotNull("userName".intern(), userName, returnObj);
		
		String id = (String)scimResponse.get("id".intern());//root
		addAttributeIfNotNull("id".intern(), id, returnObj);
		
		JSONArray emails = (JSONArray)scimResponse.get("emails".intern());
		if(emails != null){
		addAttributeIfNotNull("email".intern(), emails.get(0),returnObj);
		}
		
		String preferredLang = (String)scimResponse.get("preferredLanguage".intern());
		addAttributeIfNotNull("preferredLanguage".intern(), preferredLang, returnObj);
		
		String userType = (String)scimResponse.get("userType".intern());
		addAttributeIfNotNull("userType".intern(), userType,returnObj);
		
		JSONObject nameObj = (JSONObject)scimResponse.get("name".intern());
		if(nameObj != null){
			String firstName= (String)nameObj.get("givenName".intern());
			addAttributeIfNotNull("firstName".intern(), firstName, returnObj);
			String lastName = (String)nameObj.get("familyName".intern());
			addAttributeIfNotNull("lastName".intern(), lastName, returnObj);
		}
		
		JSONObject wso2Extension = (JSONObject)scimResponse.get("wso2Extension".intern());
		if(wso2Extension != null){
		String prefMethComm = (String)wso2Extension.get("prefMethComm".intern());
		addAttributeIfNotNull("prefMethComm".intern(), prefMethComm, returnObj);
		String secQuestion1 = (String)wso2Extension.get("secQuestion1".intern());
		addAttributeIfNotNull("secQuestion1".intern(), secQuestion1, returnObj);
		
		String secQuestion2 = (String)wso2Extension.get("secQuestion2".intern());
		addAttributeIfNotNull("secQuestion2".intern(), secQuestion2, returnObj);
		
		String secAnswer1 = (String)wso2Extension.get("secAnswer1".intern());
		addAttributeIfNotNull("secAnswer1".intern(), secAnswer1, returnObj);
		
		String secAnswer2 = (String)wso2Extension.get("secAnswer2".intern());
		returnObj.put("secAnswer2".intern(), secAnswer2);
		
		String tenantCode = (String)wso2Extension.get("giTenantId".intern());
		addAttributeIfNotNull("tenantCode".intern(), tenantCode, returnObj);
		}
		
		
		
		JSONObject meta = (JSONObject)scimResponse.get("meta".intern());//root
		if(meta != null){
			String created = (String)meta.get("created".intern());
			addAttributeIfNotNull("created".intern(), created, returnObj);
			
			String location = (String)meta.get("location".intern());
			addAttributeIfNotNull("location".intern(), location, returnObj);
			
			String lastModified = (String)meta.get("lastModified".intern());
			addAttributeIfNotNull("last_modified".intern(), lastModified, returnObj);
		}
		return returnObj;
	}

}
