package com.getinsured.iex.im.wso2.exceptions;

public class CalimValueNotSupported extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CalimValueNotSupported() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CalimValueNotSupported(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public CalimValueNotSupported(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CalimValueNotSupported(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CalimValueNotSupported(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
