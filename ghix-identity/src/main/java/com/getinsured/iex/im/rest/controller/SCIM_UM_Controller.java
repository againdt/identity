package com.getinsured.iex.im.rest.controller;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URLEncoder;
import java.util.Base64;

import org.apache.http.HttpEntity;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.wso2.charon3.core.encoder.JSONEncoder;
import org.wso2.charon3.core.objects.User;

import com.getinsured.hix.platform.util.SCIMClientConstants;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.getinsured.iex.im.scim.SCIMJSONParser;
import com.getinsured.iex.im.scim.SCIMTransformer;

@Controller
public class SCIM_UM_Controller {
	
	private String SCIM_USER_ENDPOINT;
	
	@Autowired
	private CloseableHttpClient httpClient;
	private Logger LOGGER = LoggerFactory.getLogger(SCIM_UM_Controller.class);
	private SCIMTransformer transformer = new SCIMTransformer(); 
	private JSONEncoder encoder = new JSONEncoder();
	
	
	@Value("#{configProp['scim.user.endpoint'] != null ? configProp['scim.user.endpoint'] : 'https://localhost:9443/wso2/scim/Users'}")
	public void setSCIMUserEndpoint(String userEndpoint){
		SCIM_USER_ENDPOINT = userEndpoint;
	}
	
	@RequestMapping(value = "/scim/tenant/user/update", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> updateUser(@RequestBody String userData){
		String responseBody = null;
		HttpEntity requestEntity = null;
		int responseStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
		CloseableHttpResponse httpResponse = null;
		HttpEntity responseEntity = null;
		JSONObject responseJson = null;
		SCIMJSONParser scimjsonParser = new SCIMJSONParser();
		User user = null;
		JSONParser parser = new JSONParser();
		JSONObject userDataObj = null;
		MediaType type;
		HttpPatch httput = null;
		// Payload is good
		try {
			String tenantDomain = ""; //Default Empty
			userDataObj = (JSONObject)parser.parse(userData);
			String tenantAdmin = (String)userDataObj.get("tenantAdmin".intern());
			String tenantPass = (String)userDataObj.get("tenantPassword".intern());
			String tmp = (String)userDataObj.get("tenantDomain".intern());
			if(tmp != null){
				tenantDomain = tmp;
				LOGGER.info("Received update user request for tenant ".intern()+tenantDomain);
			}
			JSONObject obj = (JSONObject)userDataObj.get("payload".intern());
			if(obj == null){
				throw new RuntimeException("No payload provided, can not update the user".intern());
			}
			String userId = (String)obj.get("id".intern());
			if(userId == null){
				throw new RuntimeException("User Id is a mandatory attribute to update a user, please check the payload".intern());
			}
			user = transformer.getScimUserForUpdate(obj);
			if(LOGGER.isTraceEnabled()){ // PII Log
				LOGGER.trace(encoder.encodeSCIMObject(user));;
			}
			httput = new HttpPatch(SCIM_USER_ENDPOINT+"/"+userId);
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Create user, using endpoint:"+SCIM_USER_ENDPOINT);		
			}
			String credentialsStr = "Basic "+Base64.getEncoder().encodeToString((tenantAdmin+"@"+tenantDomain+":"+tenantPass).getBytes());
			httput.addHeader("Authorization",credentialsStr);
			requestEntity = EntityBuilder.create()
					.setContentType(ContentType.APPLICATION_JSON)
					.setText(encoder.encodeSCIMObject(user)).build();
			httput.setEntity(requestEntity);
			httpResponse = httpClient.execute(httput);
			responseEntity = httpResponse.getEntity();
			responseStatus = httpResponse.getStatusLine().getStatusCode();
			responseJson = scimjsonParser.getJsonResponseFromEntity(responseEntity);
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("USR UPDATE RESPONSE RECEIVED FROM WSO2: ".intern()
						+ httpResponse.getStatusLine());
			}
			if(LOGGER.isTraceEnabled()){ // PII Log
				LOGGER.trace("Response:".intern()+responseJson.toJSONString());;
			}
			if (HttpStatus.OK.value() != responseStatus) {
					responseBody = responseJson.toJSONString();
			} else {
					responseBody = scimjsonParser.processCreateUserSuccessResponse((JSONObject)responseJson);
			}
			type = MediaType.APPLICATION_JSON;
		} 
		catch(ConnectException e){
			LOGGER.error("Failed to connect the server".intern(),e);
			responseBody = PlatformServiceUtil.exceptionToJson(e);
			type = MediaType.APPLICATION_JSON;
		}
		catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error encountered while processing request".intern(),e);
			responseBody = PlatformServiceUtil.exceptionToJson(e);
			type = MediaType.APPLICATION_JSON;
		} finally {
			if(httput != null){
				httput.releaseConnection();
			}
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					//Ignored
				}
			}
		}
		return PlatformServiceUtil.getResponseEntity(responseBody, HttpStatus.valueOf(responseStatus), null,type);
	}
	
	/**
	 * 
	 * @param userData is a Json String containing
	 * 1) id-> scim id of the user we are trying to update
	 * 2) userName - >  existing user name of the user being modified
	 * 3) new_user_name - > desired user name
	 * 4) existing_password - > User's existing password, its caller's responsibility to provide the correct password
	 * @return
	 * JSON response containing the new scim id
	 * if clean up failed, it will return the old scim id with instructions to manually cleanup the DB entry
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/scim/tenant/user/update/username", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> updateUserName(@RequestBody String userData) {
		String responseBody = null;
		HttpEntity requestEntity = null;
		int responseStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
		CloseableHttpResponse httpResponse = null;
		HttpEntity responseEntity = null;
		JSONObject responseJson = null;
		SCIMJSONParser scimjsonParser = new SCIMJSONParser();
		JSONParser parser = new JSONParser();
		JSONObject userDataObj = null;
		MediaType type = MediaType.APPLICATION_JSON;
		HttpPost httpost = null;
		try {
			boolean created = false;
			String tenantDomain = ""; // Default Empty
			userDataObj = (JSONObject) parser.parse(userData);
			String tenantAdmin = (String) userDataObj.get("tenantAdmin".intern());
			String tenantPass = (String) userDataObj.get("tenantPassword".intern());
			String tmp = (String) userDataObj.get("tenantDomain".intern());
			if (tmp != null) {
				tenantDomain = tmp;
				LOGGER.info("Received update user request for tenant ".intern() + tenantDomain);
			}
			JSONObject obj = (JSONObject) userDataObj.get("payload".intern());
			if (obj == null) {
				throw new RuntimeException("No payload provided, can not update the user".intern());
			}
			String userId = (String) obj.get("id".intern());
			if (userId == null) {
				throw new RuntimeException(
						"User Id is a mandatory attribute to update a user, please check the payload".intern());
			}
			String newUserName = (String) obj.get("new_user_name");
			String currentUserName = (String) obj.get("userName");
			String password = (String)obj.get("password");
			// First retrieve the user
			JSONObject existingUser = this.getUserFromWso2(userId, tenantAdmin, tenantPass, tenantDomain);
			if (existingUser != null) {
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Retrieved existing user record:\n"+existingUser.toJSONString());
				}
				String uName = (String) existingUser.get("userName");
				if (!uName.equalsIgnoreCase(currentUserName)) {
					throw new RuntimeException(
							"Existing username provided does not match with the provided user id information [found "+uName+" provided:"+currentUserName+"]");
				}
				existingUser.put("userName", newUserName);
				existingUser.remove("meta".intern()); // We don't need the metadata
				existingUser.remove("id".intern()); // Its a copy, no id required
				existingUser.put("password", password); // We want to retain the existing password
				JSONArray emailArray = (JSONArray) existingUser.get("emails".intern());
				emailArray.clear();
				emailArray.add(0, newUserName); // email and username has to be same
				// Now create the user
				httpost = new HttpPost(SCIM_USER_ENDPOINT);
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Create user, using endpoint:" + SCIM_USER_ENDPOINT);
					LOGGER.debug("User Payload:"+existingUser.toJSONString());
				}
				String credentialsStr = "Basic " + Base64.getEncoder()
						.encodeToString((tenantAdmin + "@" + tenantDomain + ":" + tenantPass).getBytes());
				httpost.addHeader("Authorization", credentialsStr);
				requestEntity = EntityBuilder.create().setContentType(ContentType.APPLICATION_JSON)
						.setText(existingUser.toJSONString()).build();
				httpost.setEntity(requestEntity);
				httpResponse = httpClient.execute(httpost);
				responseEntity = httpResponse.getEntity();
				responseStatus = httpResponse.getStatusLine().getStatusCode();
				responseJson = scimjsonParser.getJsonResponseFromEntity(responseEntity);
				if (SCIMClientConstants.USR_CREATED_INT != responseStatus) {
					responseBody = responseJson.toJSONString();
					type = MediaType.APPLICATION_JSON;
				} else {
					created = true;
					responseJson = scimjsonParser.processCreateUserSuccessResponseObject(responseJson);
					type = MediaType.APPLICATION_JSON;
				}
				type = MediaType.APPLICATION_JSON;
				if (created) {
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("Initiating user cleanup for is:"+userId);
					}
					// Now delete the user
					boolean deleted = this.deleteUser(userId, tenantAdmin, tenantPass, tenantDomain);
					if(!deleted){
						JSONObject cleanup = new JSONObject();
						cleanup.put("id".intern(), userId);
						cleanup.put("status".intern(),deleted);
						cleanup.put("message".intern(),"manual deletion required");
						responseJson.put("cleanup".intern(), cleanup);
					}
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("Clean up completed with status"+deleted);
					}
					responseBody = responseJson.toJSONString();
				}
			}
		} catch (ConnectException e) {
			LOGGER.error("Failed to connect the server".intern(), e);
			responseBody = PlatformServiceUtil.exceptionToJson(e);
			type = MediaType.APPLICATION_JSON;
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error encountered while processing request".intern(), e);
			responseBody = PlatformServiceUtil.exceptionToJson(e);
			type = MediaType.APPLICATION_JSON;
		} finally {
			if (httpost != null) {
				httpost.releaseConnection();
			}
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					// Ignored
				}
			}
		}
		return PlatformServiceUtil.getResponseEntity(responseBody, HttpStatus.valueOf(responseStatus), null, type);
	}
	
	@RequestMapping(value = "/scim/tenant/user/add", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> createUser(@RequestBody String userData){
		String responseBody = null;
		//ResponseEntity<String> response = null;
		HttpEntity requestEntity = null;
		int responseStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
		HttpPost httpost = null;
		CloseableHttpResponse httpResponse = null;
		HttpEntity responseEntity = null;
		Object responseJson = null;
		SCIMJSONParser scimjsonParser = new SCIMJSONParser();
		User user = null;
		JSONParser parser = new JSONParser();
		JSONObject userDataObj = null;
		MediaType type;
		// Payload is good
		try {
			String tenantDomain = ""; //Default Empty
			userDataObj = (JSONObject)parser.parse(userData);
			String tenantAdmin = (String)userDataObj.get("tenantAdmin".intern());
			String tenantPass = (String)userDataObj.get("tenantPassword".intern());
			String tmp = (String)userDataObj.get("tenantDomain".intern());
			if(tmp != null){
				tenantDomain = tmp;
				LOGGER.info("Received Create user request for tenant "+tenantDomain);
			}
			JSONObject obj = (JSONObject)userDataObj.get("payload".intern());
			if(obj == null){
				throw new RuntimeException("No payload provided, can not update the user".intern());
			}
			user = transformer.getScimUser((JSONObject)userDataObj.get("payload"), null);
			//System.out.println(encoder.encodeSCIMObject(user));
			httpost = new HttpPost(SCIM_USER_ENDPOINT);
			LOGGER.info("Create user, using endpoint:"+SCIM_USER_ENDPOINT);			
			String credentialsStr = "Basic "+Base64.getEncoder().encodeToString((tenantAdmin+"@"+tenantDomain+":"+tenantPass).getBytes());
			httpost.addHeader("Authorization",credentialsStr);
			requestEntity = EntityBuilder.create()
					.setContentType(ContentType.APPLICATION_JSON)
					.setText(encoder.encodeSCIMObject(user)).build();
			httpost.setEntity(requestEntity);
			httpResponse = httpClient.execute(httpost);
			responseEntity = httpResponse.getEntity();
			responseStatus = httpResponse.getStatusLine().getStatusCode();
			responseJson = scimjsonParser.getJsonResponseFromEntity(responseEntity);
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("USR CREATION RESPONSE RECEIVED FROM WSO2: "
						+ httpResponse.getStatusLine());
			}
			
			if (SCIMClientConstants.USR_CREATED_INT != responseStatus) {
				if(responseJson instanceof JSONObject){
					responseBody = ((JSONObject) responseJson).toJSONString();
					type = MediaType.APPLICATION_JSON;
				}else{
					responseBody = (String)responseJson;
					type = MediaType.TEXT_PLAIN;
				}
			} else {
				responseBody = scimjsonParser.processCreateUserSuccessResponse((JSONObject)responseJson);
				type = MediaType.APPLICATION_JSON;
			}
		} 
		catch(ConnectException e){
			LOGGER.error("Failed to connect the server",e);
			responseBody = PlatformServiceUtil.exceptionToJson(e);
			type = MediaType.APPLICATION_JSON;
		}
		catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error encountered while processing request",e);
			responseBody = PlatformServiceUtil.exceptionToJson(e);
			type = MediaType.APPLICATION_JSON;
		} finally {
			if(httpost != null){
				httpost.releaseConnection();
			}
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					//Ignored
				}
			}
		}
		return PlatformServiceUtil.getResponseEntity(responseBody, HttpStatus.valueOf(responseStatus), null,type);
	}
	
	public boolean deleteUser(String id, String tenantAdmin, String tenantPass, String tenantDomain){
		int responseStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
		HttpDelete userDelete = null;
		CloseableHttpResponse httpResponse = null;
		try {
			if(id == null){
				return false;
			}else{
				userDelete = new HttpDelete(SCIM_USER_ENDPOINT+"/"+id);
				LOGGER.info("Delete user, using endpoint:"+SCIM_USER_ENDPOINT+"/id");			
				String credentialsStr = "Basic "+Base64.getEncoder().encodeToString((tenantAdmin+"@"+tenantDomain+":"+tenantPass).getBytes());
				userDelete.addHeader("Authorization",credentialsStr);
				httpResponse = httpClient.execute(userDelete);
				responseStatus = httpResponse.getStatusLine().getStatusCode();
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("USR DELETION RESPONSE RECEIVED FROM WSO2: "
							+ httpResponse.getStatusLine());
				}
				if (HttpStatus.OK.value() != responseStatus) {
						return false;
				} else {
					return true;
				}
			}
		} 
		catch(ConnectException e){
			LOGGER.error("Failed to connect the server",e);
		}
		catch (Exception e) {
			LOGGER.error("Error encountered while processing request",e);
		} finally {
			if(userDelete != null){
				userDelete.releaseConnection();
			}
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					//Ignored
				}
			}
		}
		return false;
	}
	
	@RequestMapping(value = "/scim/tenant/user/find", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> findUser(@RequestBody  String findUserPayload){
		String responseBody = null;
		int responseStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
		HttpGet httpget = null;
		CloseableHttpResponse httpResponse = null;
		HttpEntity responseEntity = null;
		Object responseJson = null;
		SCIMJSONParser scimjsonParser = new SCIMJSONParser();
		JSONParser parser = new JSONParser();
		JSONObject requestJson = null;
		MediaType type;
		try {
			requestJson = (JSONObject) parser.parse(findUserPayload);
			String tenantDomain = ""; //Default Empty
			String tenantAdmin = (String)requestJson.get("tenantAdmin".intern());
			String tenantPass = (String)requestJson.get("tenantPassword".intern());
			String tmp = (String)requestJson.get("tenantDomain".intern());
			if(tmp != null){
				tenantDomain = tmp;
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("Received find user request for tenant "+tenantDomain);
				}
			}
			JSONObject payloadJson = (JSONObject) requestJson.get("payload".intern());
			String filterParam = (String)payloadJson.get("filterParam");
			String filterCriteria = (String)payloadJson.get("filterCriteria");
			httpget = new HttpGet(SCIM_USER_ENDPOINT+"?filter="+URLEncoder.encode(filterParam.trim()+" Eq "+filterCriteria.trim(),"UTF-8"));
			httpget.setHeader("Accept","application/json");
			String credentialsStr = "Basic "+Base64.getEncoder().encodeToString((tenantAdmin.trim()+"@".intern()+tenantDomain.trim()+":".intern()+tenantPass.trim()).getBytes());
			httpget.addHeader("Authorization",credentialsStr);
			httpResponse = httpClient.execute(httpget);
			responseEntity = httpResponse.getEntity();
			responseStatus = httpResponse.getStatusLine().getStatusCode();
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Find user response code:"+responseStatus);
			}
			responseJson = scimjsonParser.getJsonResponseFromEntity(responseEntity);
			if (responseStatus == HttpStatus.OK.value()) {
				JSONObject response = scimjsonParser.processFindUserSuccessResponse((JSONObject)responseJson);
				responseBody = response.toJSONString();
				type = MediaType.APPLICATION_JSON;
			} else {
					responseBody = ((JSONObject) responseJson).toJSONString();
					type = MediaType.APPLICATION_JSON;
			}
		} 
		catch(ConnectException e){
			LOGGER.error("Failed to connect the server",e);
			responseBody = PlatformServiceUtil.exceptionToJson(e);
			type = MediaType.APPLICATION_JSON;
		}
		catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error encountered while processing request",e);
			responseBody = PlatformServiceUtil.exceptionToJson(e);
			type = MediaType.APPLICATION_JSON;
		} finally {
			if(httpget != null){
				httpget.releaseConnection();
			}
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					//Ignored
				}
			}
		}
		return PlatformServiceUtil.getResponseEntity(responseBody, HttpStatus.valueOf(responseStatus), null,type);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/scim/tenant/user/{scimId}", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> getUser(@PathVariable("scimId")String scimId, @RequestBody String requestPayload){
		String responseBody = null;
		int responseStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
		HttpGet httpget = null;
		CloseableHttpResponse httpResponse = null;
		HttpEntity responseEntity = null;
		Object responseJson = null;
		SCIMJSONParser scimjsonParser = new SCIMJSONParser();
		JSONParser parser = new JSONParser();
		JSONObject requestJson = null;
		MediaType type;
		try {
			requestJson = (JSONObject) parser.parse(requestPayload);
			String tenantDomain = ""; //Default Empty
			String tenantAdmin = (String)requestJson.get("tenantAdmin".intern());
			String tenantPass = (String)requestJson.get("tenantPassword".intern());
			String tmp = (String)requestJson.get("tenantDomain".intern());
			if(tmp != null){
				tenantDomain = tmp;
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("Received find user request for tenant "+tenantDomain);
				}
			}
			httpget = new HttpGet(SCIM_USER_ENDPOINT+"/"+scimId);
			httpget.setHeader("Accept","application/json");
			String credentialsStr = "Basic "+Base64.getEncoder().encodeToString((tenantAdmin.trim()+"@".intern()+tenantDomain.trim()+":".intern()+tenantPass.trim()).getBytes());
			httpget.addHeader("Authorization",credentialsStr);
			httpResponse = httpClient.execute(httpget);
			responseEntity = httpResponse.getEntity();
			responseStatus = httpResponse.getStatusLine().getStatusCode();
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Find user response code:"+responseStatus);
			}
			responseJson = scimjsonParser.getJsonResponseFromEntity(responseEntity);
			if (responseStatus == HttpStatus.OK.value()) {
				JSONObject response = scimjsonParser.processGetUserSuccessResponse((JSONObject)responseJson);
				
				if(response.get("EXCEPTION") != null){
					response.put("Message", "Servive Invoation was successful but failed to process the response correctly");
					responseStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
				}
				responseBody = response.toJSONString();
				type = MediaType.APPLICATION_JSON;
			} else {
					responseBody = ((JSONObject) responseJson).toJSONString();
					type = MediaType.APPLICATION_JSON;
			}
		} 
		catch(ConnectException e){
			LOGGER.error("Failed to connect the server",e);
			responseBody = PlatformServiceUtil.exceptionToJson(e);
			type = MediaType.APPLICATION_JSON;
		}
		catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error encountered while processing request",e);
			responseBody = PlatformServiceUtil.exceptionToJson(e);
			type = MediaType.APPLICATION_JSON;
		} finally {
			if(httpget != null){
				httpget.releaseConnection();
			}
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					//Ignored
				}
			}
		}
		return PlatformServiceUtil.getResponseEntity(responseBody, HttpStatus.valueOf(responseStatus), null,type);
	}
	
	public JSONObject getUserFromWso2(String scimId, String tenantAdmin, String tenantPass, String tenantDomain){
		int responseStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
		HttpGet httpget = null;
		CloseableHttpResponse httpResponse = null;
		HttpEntity responseEntity = null;
		JSONObject responseJson = null;
		SCIMJSONParser scimjsonParser = new SCIMJSONParser();
		try {
			httpget = new HttpGet(SCIM_USER_ENDPOINT+"/"+scimId);
			httpget.setHeader("Accept","application/json");
			String credentialsStr = "Basic "+Base64.getEncoder().encodeToString((tenantAdmin.trim()+"@".intern()+tenantDomain.trim()+":".intern()+tenantPass.trim()).getBytes());
			httpget.addHeader("Authorization",credentialsStr);
			httpResponse = httpClient.execute(httpget);
			responseEntity = httpResponse.getEntity();
			responseStatus = httpResponse.getStatusLine().getStatusCode();
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Find user response code:"+responseStatus);
			}
			responseJson = scimjsonParser.getJsonResponseFromEntity(responseEntity);
			if (responseStatus == HttpStatus.OK.value()) {
					return responseJson;
			}else{
				LOGGER.error("Failed to retrieve the user with id:"+scimId);
			}
		} 
		catch(ConnectException e){
			LOGGER.error("Failed to connect the server",e);
		}
		catch (Exception e) {
			LOGGER.error("Error encountered while processing request",e);
		} finally {
			if(httpget != null){
				httpget.releaseConnection();
			}
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					//Ignored
				}
			}
		}
		return null;
	}
	
	@RequestMapping(value = "/scim/tenant/user/getuser", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<String> geScimUser(@RequestBody String userData) {
		String responseBody = null;
		int responseStatus = HttpStatus.INTERNAL_SERVER_ERROR.value();
		CloseableHttpResponse httpResponse = null;
		JSONParser parser = new JSONParser();
		JSONObject userDataObj = null;
		MediaType type = MediaType.APPLICATION_JSON;
		HttpPost httpost = null;
		try {
			String tenantDomain = ""; // Default Empty
			userDataObj = (JSONObject) parser.parse(userData);
			String tenantAdmin = (String) userDataObj.get("tenantAdmin".intern());
			String tenantPass = (String) userDataObj.get("tenantPassword".intern());
			String tmp = (String) userDataObj.get("tenantDomain".intern());
			if (tmp != null) {
				tenantDomain = tmp;
				LOGGER.info("Received get user request for tenant ".intern() + tenantDomain);
			}
			JSONObject obj = (JSONObject) userDataObj.get("payload".intern());
			if (obj == null) {
				throw new RuntimeException("No payload provided, can not update the user".intern());
			}
			String userId = (String) obj.get("id".intern());
			if (userId == null) {
				throw new RuntimeException(
						"User Id is a mandatory attribute to update a user, please check the payload".intern());
			}
		
			// First retrieve the user
			JSONObject existingUser = this.getUserFromWso2(userId, tenantAdmin, tenantPass, tenantDomain);
			if(existingUser == null){
				responseStatus = HttpStatus.NOT_FOUND.value();
				responseBody = "No user available for id:".intern()+userId;
			}else{
				responseBody = existingUser.toJSONString();
				responseStatus = HttpStatus.OK.value();
			}
		}  catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("Error encountered while processing request".intern(), e);
			responseBody = PlatformServiceUtil.exceptionToJson(e);
			type = MediaType.APPLICATION_JSON;
		} finally {
			if (httpost != null) {
				httpost.releaseConnection();
			}
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					// Ignored
				}
			}
		}
		return PlatformServiceUtil.getResponseEntity(responseBody, HttpStatus.valueOf(responseStatus), null, type);
	}
}
