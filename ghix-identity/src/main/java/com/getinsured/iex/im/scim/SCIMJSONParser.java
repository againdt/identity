/**
 * 
 */
package com.getinsured.iex.im.scim;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.util.SCIMClientConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author Biswakesh.Praharaj
 * 
 */
public class SCIMJSONParser {
	private static final Logger LOGGER = LoggerFactory.getLogger(SCIMJSONParser.class);

	
	@SuppressWarnings("unchecked")
	public String processCreateUserSuccessResponse(JSONObject userJsonObject){
		String id = null;
		String userName = null;
		JSONObject obj = new JSONObject();
		id = (String) userJsonObject.get(SCIMClientConstants.ID_ATTR);
		userName = (String) userJsonObject.get(SCIMClientConstants.USER_NAME_ATTR);
		obj.put(SCIMClientConstants.ID_ATTR.intern(), id);
		obj.put(SCIMClientConstants.USER_NAME_ATTR, userName);
		return obj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject processCreateUserSuccessResponseObject(JSONObject userJsonObject){
		String id = null;
		String userName = null;
		JSONObject obj = new JSONObject();
		id = (String) userJsonObject.get(SCIMClientConstants.ID_ATTR);
		userName = (String) userJsonObject.get(SCIMClientConstants.USER_NAME_ATTR);
		obj.put(SCIMClientConstants.ID_ATTR.intern(), id);
		obj.put(SCIMClientConstants.USER_NAME_ATTR, userName);
		return obj;
	}
	
//	public AccountUser processFetchUserResponse(JSONObject userJsonObject) throws GIException {
//		String id = null;
//		String userName = null;
//		String firstName = null;
//		String lastName = null;
//		String mail = null;
//		String phoneNumber = null;
//		String prefMethComm = null;
//		String preferredLanguage = null;
//		String securityQuestion = null;
//		String securityAnswer = null;
//		String ridpFlag = null;
//		JSONArray jsonArray = null;
//		JSONObject childJsonObject = null;
//		JSONObject wso2ExtensionObject = null;
//		AccountUser accountUser = null;
//		try {
//			accountUser = new AccountUser();
//			jsonArray = (JSONArray)userJsonObject.get("Resources");
//			if (null != jsonArray) {
//				// There is always one user available for a given email id.
//				// Hence, one can safely fetch the only element in the array
//				childJsonObject = (JSONObject)jsonArray.get(0);
//				id = (String) childJsonObject.get(SCIMClientConstants.ID_ATTR);
//				userName = (String) childJsonObject.get(SCIMClientConstants.USER_NAME_ATTR);
//				
//				wso2ExtensionObject = (JSONObject) childJsonObject
//						.get(SCIMClientConstants.WSO2_EXTENSION_ATTR);
//				firstName = (String) wso2ExtensionObject
//						.get(SCIMClientConstants.FIRST_NAME_ATTR);
//				lastName = (String) wso2ExtensionObject
//						.get(SCIMClientConstants.LAST_NAME_ATTR);
//				mail = (String) wso2ExtensionObject.get(SCIMClientConstants.MAIL_ATTR);
//				phoneNumber = (String) wso2ExtensionObject
//						.get(SCIMClientConstants.PHONE_NUMBER_ATTR);
//				prefMethComm = (String) wso2ExtensionObject
//						.get(SCIMClientConstants.PREF_METH_COMM_ATTR);
//				preferredLanguage = (String) wso2ExtensionObject
//						.get(SCIMClientConstants.PREFERRED_LANGUAGE_ATTR);
//				securityQuestion = (String) wso2ExtensionObject
//						.get(SCIMClientConstants.SECURITY_QUESTION_ATTR);
//				securityAnswer = (String) wso2ExtensionObject
//						.get(SCIMClientConstants.SECURITY_ANSWER_ATTR);
//				ridpFlag = (String) wso2ExtensionObject
//						.get(SCIMClientConstants.RIDP_FLAG_ATTR);
//				
//				accountUser.setExtnAppUserId(id);
//				accountUser.setUserName(userName);
//				accountUser.setFirstName(firstName);
//				accountUser.setLastName(lastName);
//				accountUser.setEmail(mail);
//				accountUser.setPhone(phoneNumber);
//				accountUser.setCommunicationPref(preferredLanguage);
//				accountUser.setSecurityQuestion1(securityQuestion);
//				accountUser.setSecurityAnswer1(securityAnswer);
//			}
//			
//			if (LOGGER.isInfoEnabled()) {
//				LOGGER.info("USER CREATED SUCCESSFULLY");
//			}
//			
//		} catch (Exception e) {
//			LOGGER.error("ERR: WHILE PARSING DATA: ", e);
//			throw new GIException("Error while processing SCIM response",e);
//		}
//		return accountUser;
//	}
	
	
	
	

//	/**
//	 * Method which parses the List groups response to fetch
//	 * 
//	 * @param response
//	 *            String containing the list groups response
//	 * @return List containing names of groups
//	 */
//	@SuppressWarnings("rawtypes")
//	public List<String> processListGroupsResponse(String response) {
//		JSONParser jsonParser = null;
//		JSONObject jsonObject = null;
//		JSONArray jsonArray = null;
//		Iterator jsonIterator = null;
//		List<String> groups = null;
//		try {
//			jsonParser = new JSONParser();
//			jsonObject = (JSONObject) jsonParser.parse(response);
//			jsonArray = (JSONArray) jsonObject.get("Resources");
//			if (null != jsonArray && !jsonArray.isEmpty()) {
//				jsonIterator = jsonArray.iterator();
//				groups = new ArrayList<String>();
//				while (jsonIterator.hasNext()) {
//					JSONObject groupJsonObject = (JSONObject) jsonIterator
//							.next();
//					String groupName = (String) groupJsonObject
//							.get("displayName");
//					groups.add(groupName);
//				}
//			}
//		} catch (Exception e) {
//			LOGGER.error("ERR: WHILE FETCHING GRPS: ", e);
//		}
//		return groups;
//	}

	/**
	 * Caller needs to check for null return
	 * 
	 * @param entity
	 * @return
	 * @throws GIException
	 */
	public JSONObject getJsonResponseFromEntity(HttpEntity entity)
			throws GIException {
		Exception ex = null;
		JSONObject obj = null;
		try {
			JSONParser parser = new JSONParser();
			InputStream is = entity.getContent();
			obj = (JSONObject) parser.parse(new BufferedReader(
					new InputStreamReader(is)));
		} catch (IOException ie) {
			ex = ie;
		} catch (ParseException e) {
			ex = e;
		}
		if (ex != null) {
			throw new GIException(
					"Failed to process JSON response received from WSO2", ex);
		}
		return obj;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject processGetUserSuccessResponse(JSONObject responseJson) {
		SCIMTransformer transformer = new SCIMTransformer();
		JSONObject userRecord = transformer.getUserDetailsFromSCIMResponse(responseJson);
		return userRecord;
	}

	@SuppressWarnings("unchecked")
	public JSONObject processFindUserSuccessResponse(JSONObject responseJson) {
		JSONArray jsonArray = (JSONArray)responseJson.get("Resources");
		if(jsonArray == null){
			LOGGER.error("Bad Response:"+responseJson.toJSONString());
		}
		int len = jsonArray.size();
		JSONObject response = new JSONObject();
		response.put("num_results", len);
		if(len  > 0){
			SCIMTransformer transformer = new SCIMTransformer();
			JSONArray results = new JSONArray();
			JSONObject userRecord = null;
			for(Object obj:jsonArray){
				userRecord = transformer.getUserDataFromSCIMResponse((JSONObject)obj);
				results.add(userRecord);
			}
			response.put("results", results);
		}
		return response;
	}
}
