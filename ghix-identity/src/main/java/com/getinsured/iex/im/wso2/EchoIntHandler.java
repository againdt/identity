package com.getinsured.iex.im.wso2;


import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import com.getinsured.iex.im.wso2.availability.echo.EchoInt;
import com.getinsured.iex.im.wso2.availability.echo.EchoIntResponse;
import com.getinsured.iex.im.wso2.availability.echo.ObjectFactory;
import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;

public class EchoIntHandler extends AbstractServiceHandler {
	private static final ObjectFactory factory = new ObjectFactory();
	private EchoIntResponse response = null;
	@Override
	public Object getRequestpayLoad() throws HubServiceException,
			HubMappingException {
		Long id = null;
		EchoInt echoInt = null;
		JAXBElement<EchoInt> requestPayLoad;
		try{
			JSONParser parser = new JSONParser();
			JSONObject obj = (JSONObject) parser.parse(getJsonInput());
			id = (Long) obj.get("echo");
			QName echoIntQname  = new QName("http://echo.services.core.carbon.wso2.org", "echoInt");
			echoInt = factory.createEchoInt();
			echoInt.setIn(id.intValue());
			requestPayLoad = new JAXBElement<EchoInt>(echoIntQname, EchoInt.class, echoInt);
		}catch(ClassCastException | ParseException ce){
			throw new HubServiceException("Failed to read the JSON payload",ce);
		} 
		
		return requestPayLoad;
	}

	@Override
	public String handleResponse(Object responseObj) throws HubServiceException {
		if(responseObj == null){
			throw new HubServiceException("Handle response called with null response object");
		}
		try{
			response = (EchoIntResponse)responseObj;
		}catch(ClassCastException ce){
			throw new HubServiceException("Expected Int response, received "+response.getClass().getName());
		}
		return Integer.toString(response.getReturn());
	}

	@Override
	public RequestStatus getRequestStatus() {
		if(response != null){
			return RequestStatus.SUCCESS;
		}
		return RequestStatus.FAILED;
	}

	@Override
	public String getServiceIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Marshaller getServiceMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		// TODO Auto-generated method stub
		return null;
	}

}
