package com.getinsured.identity.cache;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.identity.model.AccountUser;
import com.getinsured.identity.model.Role;
import com.getinsured.identity.model.RolePermission;
import com.getinsured.identity.model.UserRole;
import com.getinsured.identity.repository.AccountUserRepository;
import com.getinsured.identity.repository.RoleRepository;

@Component
public class UserPermissionProxy {
	
	@Autowired
	private AccountUserRepository userRepo;
	
	@Autowired
	private RoleRepository rolesRepo;
	
	private Logger logger = LoggerFactory.getLogger(UserPermissionProxy.class);
	
	private Map<String, RolePermissionCache> rolePermissions = Collections.synchronizedMap(new HashMap<String, RolePermissionCache>());

	public Set<RolePermission> getRolePermission(String roleName){
		RolePermissionCache rpc= this.rolePermissions.get(roleName);
		Set<RolePermission> rp = null;
		if(rpc == null){
			Role role= this.rolesRepo.findRolesByName(roleName);
			rp = role.getRolePermissions();
			// Create the cache
			this.rolePermissions.put(roleName, new RolePermissionCache(rp));
		}else{
			//We have hit the cache
			rp = rpc.getPermissions();
			if(rp == null){
				//cache expired
				Role role= this.rolesRepo.findRolesByName(roleName);
				rp = role.getRolePermissions();
				//Update the cache
				this.rolePermissions.put(roleName, new RolePermissionCache(rp));
			}
		}
		return rp;
	}
	
	public HashMap<String,Object> getUserPermissionsByExternalAppUserId(String externalAppUserId){
		HashMap<String, Object> userContext = null;
		AccountUser user = this.userRepo.findByExtnAppUserId(externalAppUserId);
		if(user != null){
			userContext = new HashMap<>();
			userContext.put("tenantId", user.getTenantId());
			Set<RolePermission> permissions = this.getUserPermissions(user, userContext);
			userContext.put("permissions", permissions);
		}
		return userContext;
	}
	
	public HashMap<String,Object> getUserPermissionsForTenant(String userName, int tenantId){
		HashMap<String, Object> userContext = null;
		AccountUser user = this.userRepo.findByUserNameAndTenantId(userName, tenantId);
		if(user != null){
			userContext = new HashMap<>();
			userContext.put("tenantId", user.getTenantId());
			Set<RolePermission> permissions = this.getUserPermissions(user, userContext);
			userContext.put("permissions", permissions);
		}
		return userContext;
	}
	
	public Set<RolePermission> getUserPermissions(AccountUser user, HashMap<String, Object> userContext){
		if(user == null){
			return null;
		}
		
		logger.info("Trying to retrieve permission for user:"+user.getUserName()+" ID:"+user.getExtnAppUserId());
		Set<RolePermission> permissions = new HashSet<RolePermission>(); 
		if(!user.getStatus().equalsIgnoreCase("active")){
			throw new RuntimeException("Permission Denied:User is not active");
		}
		Set<UserRole> userRoles = user.getUserRole();
		Role tmpRole = null;
		for(UserRole userRole: userRoles){
			if(userRole.getIsActive() == 'N'){
				continue;
			}
			tmpRole = userRole.getRole();
			if(tmpRole.getMfaEnabled().equalsIgnoreCase("Y")){
				userContext.put("mfa_enabled", true);
			}
			permissions.addAll(tmpRole.getRolePermissions());
		}
		return permissions;
	}
}
