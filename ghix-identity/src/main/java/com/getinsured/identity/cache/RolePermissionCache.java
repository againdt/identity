package com.getinsured.identity.cache;

import java.util.Set;

import com.getinsured.identity.model.RolePermission;

public class RolePermissionCache {
	private long lastAccessedTime = -1;
	private static final long expiryTime = 2*60*1000;
	private Set<RolePermission> rolePermission = null;
	
	public RolePermissionCache(Set<RolePermission> rolePermissions) {
		this.rolePermission = rolePermissions;
		this.lastAccessedTime = System.currentTimeMillis();
	}
	
	public boolean isExpired(){
		if((System.currentTimeMillis() - this.lastAccessedTime) > expiryTime){
			return true;
		}
		return false;
	}
	
	Set<RolePermission> getPermissions(){
		if(this.isExpired()){
			return null;
		}
		this.lastAccessedTime = System.currentTimeMillis();
		return this.rolePermission;
	}
}
