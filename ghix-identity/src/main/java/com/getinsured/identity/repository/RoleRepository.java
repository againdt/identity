package com.getinsured.identity.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.getinsured.identity.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	@Query("select r from Role r where r.name=:name")
	Role findRolesByName(@Param("name")String name);
}

