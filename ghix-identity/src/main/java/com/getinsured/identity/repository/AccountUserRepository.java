package com.getinsured.identity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.identity.model.AccountUser;

@Repository
public interface AccountUserRepository extends JpaRepository<AccountUser, Long> {
	AccountUser findByExtnAppUserId(String extAppUserId);
	
	@Query("select u from AccountUser u where u.userName=:userName and u.tenantId=:tenantId")
	AccountUser findByUserNameAndTenantId(@Param("userName") String userName, @Param("tenantId") int tenantId);
}

