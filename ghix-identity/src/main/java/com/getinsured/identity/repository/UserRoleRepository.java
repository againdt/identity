package com.getinsured.identity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.identity.model.UserRole;


public interface UserRoleRepository extends JpaRepository<UserRole, Integer>{
	@Query("select ur from UserRole ur where ur.user.id=:userId")
	List<UserRole> findAllUserRolesByUserId(@Param("userId")Integer userId);
	
	@Query("select ur from UserRole ur where ur.user.extnAppUserId=:externalAppId")
	List<UserRole> findAllUserRolesByExtarnalId(@Param("externalAppId") String externalAppId);
}
