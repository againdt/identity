package com.getinsured.identity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.identity.model.RolePermission;


public interface RolePermissionRepository extends JpaRepository<RolePermission, Integer> {
	
}
