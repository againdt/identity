package com.getinsured.identity.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity 
@Table(name="role_permissions")
public class RolePermission implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RolePermission_Seq")
	@SequenceGenerator(name = "RolePermission_Seq", sequenceName = "role_permissions_seq", allocationSize = 1)
	private Integer id;
	
	//uni-directional many-to-one association to Role
    @ManyToOne
    @JoinColumn(name="role_id")
	private Role role;
    
	//uni-directional many-to-one association to Role
    @ManyToOne
    @JoinColumn(name="permission_id")
	private Permission permission;
    
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="CREATION_TIMESTAMP")
	private Date created;

    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="LAST_UPDATE_TIMESTAMP")
	private Date updated;
    
    public RolePermission(){
    	
    }

	public Integer getId() {
		return id;
	}

	public Role getRole() {
		return role;
	}

	public Permission getPermission() {
		return permission;
	}

	public Date getCreated() {
		return created;
	}

	public Date getUpdated() {
		return updated;
	}
	
	@PreUpdate
	public void preUpdate(){
		throw new RuntimeException("This Object is a read only object");
	}
	
	@PrePersist
	public void prePersist(){
		throw new RuntimeException("This Object is a read only object");
	}
}