package com.getinsured.identity.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity 
@Table(name="permissions")
public class Permission implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public static enum DefaultFlag{
		Y,N
	};

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Permission_Seq")
	@SequenceGenerator(name = "Permission_Seq", sequenceName = "permissions_seq", allocationSize = 1)
	private Integer id;
	
	@Column(name="name",unique = true,nullable = false)
	private String name;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="IS_DEFAULT")
	@Enumerated(EnumType.STRING)
	private DefaultFlag isDefault; 
	
	public Permission(){
		
	}

	
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public DefaultFlag getIsDefault() {
		return isDefault;
	}


	public void setIsDefault(DefaultFlag isDefault) {
		this.isDefault = isDefault;
	}


	@PreUpdate
	public void preUpdate(){
		throw new RuntimeException("This Object is a read only object");
	}
	
	@PrePersist
	public void prePersist(){
		throw new RuntimeException("This Object is a read only object");
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Permission other = (Permission) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	

	
}