package com.getinsured.identity.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

@Entity
@Table(name="user_roles", uniqueConstraints= @UniqueConstraint(columnNames={"role_id", "user_id","default_role"}))
public class UserRole implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserRole_Seq")
	@SequenceGenerator(name = "UserRole_Seq", sequenceName = "user_roles_seq", allocationSize = 1)
	private int id;

	//uni-directional many-to-one association to Role
    @ManyToOne
    @JoinColumn(name="role_id")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private Role role;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name="user_id")
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    private AccountUser user;

    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="CREATION_TIMESTAMP")
	private Date created;

    @Temporal( TemporalType.TIMESTAMP)
    @Column(name="LAST_UPDATE_TIMESTAMP")
	private Date updated;

    @Column(name="default_role")
	private char roleFlag;

    @Column(name="Is_Active")
	private char isActive;

    public UserRole(){

    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public AccountUser getUser() {
		return user;
	}

	public void setUser(AccountUser user) {
		this.user = user;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public char getRoleFlag() {
		return roleFlag;
	}

	public void setRoleFlag(char roleFlag) {
		this.roleFlag = roleFlag;
	}

	public char getIsActive() {
		return isActive;
	}

	public void setIsActive(char isActive) {
		this.isActive = isActive;
	}
	
	@PreUpdate
	public void preUpdate(){
		throw new RuntimeException("Read Only Object, updates not allowed");
	}
	
	@PrePersist
	public void prePersist(){
		throw new RuntimeException("Read Only Object, updates not allowed");
	}
}
