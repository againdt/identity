package com.getinsured.identity.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="users")
public class AccountUser implements Serializable,Cloneable{
	private static final long serialVersionUID = 1L;

	public enum user_status
	{
		Activate("Active"), Deactivate("Inactive"), InactiveDormant("Inactive-dormant"), Pending("Pending");

		private String user_status;

		private user_status(String status) {

			this.user_status = status;
		}
		
		public String value(){
			return user_status;
		}
	}

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AccountUser_Seq")
	@SequenceGenerator(name = "AccountUser_Seq", sequenceName = "users_seq", allocationSize = 1)
	private int id;

	@Column(name="uuid",unique = true)
	private String uuid;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<UserRole> userRole = new HashSet<UserRole>();

	
	@Column(name="username",unique = true,nullable = false)
	private String userName;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "title")
	private String title;

	
	@Column(name="security_question_1")
	private String securityQuestion1;

	
	@Column(name="security_answer_1")
	private String securityAnswer1;

	@Column(name="security_question_2")
	private String securityQuestion2;

	@Column(name="security_answer_2")
	private String securityAnswer2;

	@Column(name="security_question_3")
	private String securityQuestion3;

	@Column(name="security_answer_3")
	private String securityAnswer3;

	@Column(name="pin")
	private String pin;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pin_expiration")
	private Date pinexpiration;

	@Column(name="email")
	private String email;

	@Column(name="phone", length=10)
	private String phone;

	@Column(name="recovery",unique = true,nullable = false)
	private String recovery;

	@Column(name="communication_pref")
	private String communicationPref;

	@Column(name="confirmed")
	private int confirmed;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="CREATION_TIMESTAMP")
	private Date created;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATE_TIMESTAMP")
	private Date updated;

	@Column(name="pass_recovery_token" ,length=4000)
	private String passwordRecoveryToken;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pass_recovery_token_expiration")
	private Date passwordRecoveryTokenExpiration;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="lastLogin")
	private Date lastLogin;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="startDate")
	private Date startDate;

	@Temporal( TemporalType.TIMESTAMP)
	@Column(name="endDate")
	private Date endDate;


	@Column(name="ISSUPERUSER")
	private Integer isSuperUser;

	@Column(name="EXTN_APP_USERID",unique = true)
	private String extnAppUserId;

	@Column(name = "TENANT_ID")
	private Long tenantId;

	@Column(name="status")
	@JsonIgnore
	private String status;

	@Column(name = "ffm_user_id")
	private String ffmUserId;

	@Column(name = "user_npn")
	private String userNPN;

	@Column(name="retry_count")
	private int retryCount;

	@Column(name="sec_que_retry_count")
	private int secQueRetryCount;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="PWD_LAST_UPDATED")
	private Date PasswordLastUpdatedTimeStamp;
	
	@Column(name = "LAST_UPDATED_BY")
    private Integer lastUpdatedBy;
	
	public AccountUser() {
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Set<UserRole> getUserRole() {
		return userRole;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSecurityQuestion1() {
		return securityQuestion1;
	}

	public void setSecurityQuestion1(String securityQuestion1) {
		this.securityQuestion1 = securityQuestion1;
	}

	public String getSecurityAnswer1() {
		return securityAnswer1;
	}

	public void setSecurityAnswer1(String securityAnswer1) {
		this.securityAnswer1 = securityAnswer1;
	}

	public String getSecurityQuestion2() {
		return securityQuestion2;
	}

	public void setSecurityQuestion2(String securityQuestion2) {
		this.securityQuestion2 = securityQuestion2;
	}

	public String getSecurityAnswer2() {
		return securityAnswer2;
	}

	public void setSecurityAnswer2(String securityAnswer2) {
		this.securityAnswer2 = securityAnswer2;
	}

	public String getSecurityQuestion3() {
		return securityQuestion3;
	}

	public void setSecurityQuestion3(String securityQuestion3) {
		this.securityQuestion3 = securityQuestion3;
	}

	public String getSecurityAnswer3() {
		return securityAnswer3;
	}

	public void setSecurityAnswer3(String securityAnswer3) {
		this.securityAnswer3 = securityAnswer3;
	}

	public String getPin() {
		return pin;
	}
	
	public void setPin(String pin) {
		this.pin = pin;
	}

	public Date getPinexpiration() {
		return pinexpiration;
	}

	public void setPinexpiration(Date pinexpiration) {
		this.pinexpiration = pinexpiration;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRecovery() {
		return recovery;
	}

	public void setRecovery(String recovery) {
		this.recovery = recovery;
	}

	public String getCommunicationPref() {
		return communicationPref;
	}

	public void setCommunicationPref(String communicationPref) {
		this.communicationPref = communicationPref;
	}

	public int getConfirmed() {
		return confirmed;
	}

	public void setConfirmed(int confirmed) {
		this.confirmed = confirmed;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public String getPasswordRecoveryToken() {
		return passwordRecoveryToken;
	}

	public void setPasswordRecoveryToken(String passwordRecoveryToken) {
		this.passwordRecoveryToken = passwordRecoveryToken;
	}

	public Date getPasswordRecoveryTokenExpiration() {
		return passwordRecoveryTokenExpiration;
	}

	public void setPasswordRecoveryTokenExpiration(Date passwordRecoveryTokenExpiration) {
		this.passwordRecoveryTokenExpiration = passwordRecoveryTokenExpiration;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getIsSuperUser() {
		return isSuperUser;
	}

	public void setIsSuperUser(Integer isSuperUser) {
		this.isSuperUser = isSuperUser;
	}

	public String getExtnAppUserId() {
		return extnAppUserId;
	}

	public void setExtnAppUserId(String extnAppUserId) {
		this.extnAppUserId = extnAppUserId;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFfmUserId() {
		return ffmUserId;
	}

	public void setFfmUserId(String ffmUserId) {
		this.ffmUserId = ffmUserId;
	}

	public String getUserNPN() {
		return userNPN;
	}

	public void setUserNPN(String userNPN) {
		this.userNPN = userNPN;
	}

	public int getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	public int getSecQueRetryCount() {
		return secQueRetryCount;
	}

	public void setSecQueRetryCount(int secQueRetryCount) {
		this.secQueRetryCount = secQueRetryCount;
	}

	public Date getPasswordLastUpdatedTimeStamp() {
		return PasswordLastUpdatedTimeStamp;
	}

	public void setPasswordLastUpdatedTimeStamp(Date passwordLastUpdatedTimeStamp) {
		PasswordLastUpdatedTimeStamp = passwordLastUpdatedTimeStamp;
	}

	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@PreUpdate
	public void preUpdate(){
		throw new RuntimeException("This Object is a read only object");
	}
	
	@PrePersist
	public void prePersist(){
		throw new RuntimeException("This Object is a read only object");
	}
}

