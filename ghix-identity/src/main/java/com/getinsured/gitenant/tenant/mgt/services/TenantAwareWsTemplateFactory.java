package com.getinsured.gitenant.tenant.mgt.services;

import java.util.HashMap;

import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import com.getinsured.iex.hub.platform.security.TenantAwareMessageSender;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Component
public class TenantAwareWsTemplateFactory{
	
	@Autowired
	public SaajSoapMessageFactory wso2MessageFactory;
	@Autowired
	public Jaxb2Marshaller wso2UserServiceMarshaller53;
	@Autowired
	public Jaxb2Marshaller wso2ServiceMarshaller;
	@Autowired
	public CloseableHttpClient iexHttpClient;
	@Autowired 
	private Jaxb2Marshaller wso2TenantServiceMarshaller;
	
	@Value("#{configProp['wso2.service.Url'] != null ? configProp['wso2.service.Url'] : 'https://sso.ghixqa.com:9443/services/RemoteUserStoreManagerService.RemoteUserStoreManagerServiceHttpsSoap11Endpoint'}")
	public String userServiceUri;
	
	@Value("#{configProp['wso2.tenant.serviceUrl'] != null ? configProp['wso2.tenant.serviceUrl'] : 'https://sso.ghixqa.com:9443/services/TenantMgtAdminService.TenantMgtAdminServiceHttpsSoap11Endpoint'}")
	public String tenantServiceUri;
	
	@Value("#{configProp['httpBasicAuthUser']}")
	public String defaultHttpBasicAuthUser = null;
	
	@Value("#{configProp['httpBasicAuthPass']}")
	public String defaultHttpBasicAuthPass = null;
	
	private Logger logger = LoggerFactory.getLogger(TenantAwareWsTemplateFactory.class);

	private static HashMap<String,TenantContext> tenantTemplates = new HashMap<>();
	
	/**
	 * Uses the WSO2 root ("carbon.super") credentials available from configuration
	 * Make sure credentials provided here belongs to a user with appropriate privileges 
	 * granted on WSO2 will return "5.0" compliant definitions
	 * @return
	 */
	public WebServiceTemplate getUserServiceTemplate(){
		String tenantId = "carbon.super.user".intern();
		return this.getUserServiceTemplate(tenantId, this.defaultHttpBasicAuthUser, this.defaultHttpBasicAuthPass, null);
	}
	
	/**
	 * Please make sure you pass the correct the correct WSO2 version here, supported versions are 5.0 and 5.3
	 * Passing anything other that "5.3" will load WSO2 5.0 XSD definitions, providing null will default to "5.3" 
	 * @param version
	 * @return
	 */
	public WebServiceTemplate getUserServiceTemplate(String version){
		String tenantId = "carbon.super.user".intern();
		if(version == null){
			logger.warn("No version information provided, Assuming target WSO2 version 5.3");
			version = "5.3";
		}
		return this.getUserServiceTemplate(tenantId, this.defaultHttpBasicAuthUser, this.defaultHttpBasicAuthPass, version);
	}
	
	/**
	 * Uses default WSO2 user name and password available from the configuration
	 * Make sure credentials provided here have the appropriate privileges available
	 * on WSO2
	 * @return
	 */
	public WebServiceTemplate getTenantServiceTemplate(){
		String tenantId = "carbon.super.tenant".intern();
		TenantContext ctx = tenantTemplates.get(tenantId);
		WebServiceTemplate temp = null;
		if(ctx != null){
			if(!ctx.match(this.defaultHttpBasicAuthUser, this.defaultHttpBasicAuthPass)){
				logger.info("Credentials changed, invalidating existing WS template cache for tenant domain:".intern()+tenantId);
				tenantTemplates.remove(tenantId);
			}else{
				temp = ctx.wsTemplate;
				logger.info("Target WSO2 Environment:"+ctx.versionInfo);
			}
		}
		if(temp != null){
			logger.info("Returning cached template for tenant:".intern()+tenantId);
			return temp;
		}
		temp = new WebServiceTemplate();
		temp.setMarshaller(this.wso2TenantServiceMarshaller);
		temp.setUnmarshaller(this.wso2TenantServiceMarshaller);
		temp.setDefaultUri(this.tenantServiceUri);
		temp.setMessageFactory(wso2MessageFactory);
		TenantAwareMessageSender tenantMessageSender = new TenantAwareMessageSender(tenantId, this.defaultHttpBasicAuthUser, this.defaultHttpBasicAuthPass);
		tenantMessageSender.setHttpClient(iexHttpClient);
		temp.setMessageSender(tenantMessageSender);
		TenantContext context = new TenantContext(this.defaultHttpBasicAuthUser, this.defaultHttpBasicAuthPass, temp,"5.3".intern());
		tenantTemplates.put(tenantId, context);
		return temp;
	}

	public WebServiceTemplate getUserServiceTemplate(String targetTenantId, String adminName, String adminPasword, String version){
		TenantContext ctx = tenantTemplates.get(targetTenantId);
		String versionInfo = null;
		WebServiceTemplate temp = null;
		if(ctx != null){
			if(!ctx.match(adminName, adminPasword)){
				logger.info("Credentials changed, invalidating existing WS template cache for tenant domain:".intern()+targetTenantId);
				tenantTemplates.remove(targetTenantId);
			}else{
				temp = ctx.wsTemplate;
				logger.info("Target WSO2 Environment:"+ctx.versionInfo);
			}
		}
		if(temp != null){
			logger.info("returning cached template for tenant:".intern()+targetTenantId);
			return temp;
		}
		temp = new WebServiceTemplate();
		temp.setMessageFactory(wso2MessageFactory);
		if(version != null && version.equalsIgnoreCase("5.3")){
			logger.info("Using WSO2 Version 5.3 as target".intern());
			versionInfo = "5.3";
			temp.setMarshaller(wso2UserServiceMarshaller53);
			temp.setUnmarshaller(wso2UserServiceMarshaller53);
		}else{
			// Support for WSO2 5.0
			logger.info("Using WSO2 Version 5.0 as target".intern());
			versionInfo = "5.0";
			temp.setMarshaller(wso2ServiceMarshaller);
			temp.setUnmarshaller(wso2ServiceMarshaller);
		}
		TenantAwareMessageSender tenantMessageSender = new TenantAwareMessageSender(targetTenantId, adminName, adminPasword);
		tenantMessageSender.setHttpClient(iexHttpClient);
		temp.setMessageSender(tenantMessageSender);
		temp.setDefaultUri(userServiceUri);
		TenantContext context = new TenantContext(adminName, adminPasword, temp, versionInfo);
		tenantTemplates.put(targetTenantId, context);
		return temp;
	}
	
	private class TenantContext{
		WebServiceTemplate wsTemplate;
		private String credsHash;
		String versionInfo;
		TenantContext(String userName, String password, WebServiceTemplate template, String versionInfo){
			this.credsHash = PlatformServiceUtil.getHash(userName+password);
			this.versionInfo = versionInfo;
			this.wsTemplate = template;
		}
		boolean match(String userName, String password){
			logger.info("Checking if previous credentials are valid".intern());
			return this.credsHash.equals(PlatformServiceUtil.getHash(userName+password));
		}
	}
}
